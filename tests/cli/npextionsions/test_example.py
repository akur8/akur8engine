"""Tests for Example Npextension."""

from akr.utils import test

class ExampleNpextensionTestCase(test.AKRTestCase):
    def test_load_example_npextionsion(self):
        self.app.setup()
        self.app.npextionsion.load_npextionsion('example')
