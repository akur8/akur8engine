from akr.utils import test
from akr.cli.main import get_test_app


class CliTestCaseSite(test.AKRTestCase):

    def test_akr_cli(self):
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_update_html(self):
        self.app = get_test_app(argv=['site', 'update', 'example2.com',
                                      '--html'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_update_php(self):
        self.app = get_test_app(argv=['site', 'update', 'example1.com',
                                      '--php'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_update_mysql(self):
        self.app = get_test_app(argv=['site', 'update', 'example1.com',
                                      '--html'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_update_ik(self):
        self.app = get_test_app(argv=['site', 'update', 'example5.com',
                                      '--ik'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_update_iksubdir(self):
        self.app = get_test_app(argv=['site', 'update', 'example4.com',
                                      '--iksubdir'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_update_iksubdomain(self):
        self.app = get_test_app(argv=['site', 'update', 'example7.com',
                                      '--iksubdomain'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_update_w3tc(self):
        self.app = get_test_app(argv=['site', 'update', 'example8.com',
                                      '--w3tc'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_update_ikfc(self):
        self.app = get_test_app(argv=['site', 'update', 'example9.com',
                                      '--ikfc'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_update_iksc(self):
        self.app = get_test_app(argv=['site', 'update', 'example6.com',
                                      '--iksc'])
        self.app.setup()
        self.app.run()
        self.app.close()
