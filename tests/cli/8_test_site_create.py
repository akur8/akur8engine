from akr.utils import test
from akr.cli.main import get_test_app


class CliTestCaseSite(test.AKRTestCase):

    def test_akr_cli(self):
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_create_html(self):
        self.app = get_test_app(argv=['site', 'create', 'example1.com',
                                      '--html'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_create_php(self):
        self.app = get_test_app(argv=['site', 'create', 'example2.com',
                                      '--php'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_create_mysql(self):
        self.app = get_test_app(argv=['site', 'create', 'example3.com',
                                      '--mysql'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_create_ik(self):
        self.app = get_test_app(argv=['site', 'create', 'example4.com',
                                      '--ik'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_create_iksubdir(self):
        self.app = get_test_app(argv=['site', 'create', 'example5.com',
                                      '--iksubdir'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_create_iksubdomain(self):
        self.app = get_test_app(argv=['site', 'create', 'example6.com',
                                      '--iksubdomain'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_create_w3tc(self):
        self.app = get_test_app(argv=['site', 'create', 'example7.com',
                                      '--w3tc'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_create_ikfc(self):
        self.app = get_test_app(argv=['site', 'create', 'example8.com',
                                      '--ikfc'])
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_create_iksc(self):
        self.app = get_test_app(argv=['site', 'create', 'example9.com',
                                      '--iksc'])
        self.app.setup()
        self.app.run()
        self.app.close()
