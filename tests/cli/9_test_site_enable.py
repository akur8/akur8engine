from akr.utils import test
from akr.cli.main import get_test_app


class CliTestCaseSite(test.AKRTestCase):

    def test_akr_cli(self):
        self.app.setup()
        self.app.run()
        self.app.close()

    def test_akr_cli_site_enable(self):
        self.app = get_test_app(argv=['site', 'enable', 'example2.com'])
        self.app.setup()
        self.app.run()
        self.app.close()
