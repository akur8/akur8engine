from akr.core.shellexec import AKRShellExec
from akr.core.variables import AKRVariables
import os


def check_fqdn(self, akr_host):
    """FQDN check with Akur8Engine, for mail server hostname must be FQDN"""
    # akr_host=os.popen("hostname -f | tr -d '\n'").read()
    if '.' in akr_host:
        AKRVariables.akr_fqdn = akr_host
        with open('/etc/hostname', encoding='utf-8', mode='w') as hostfile:
            hostfile.write(akr_host)

        AKRShellExec.cmd_exec(self, "sed -i \"1i\\127.0.0.1 {0}\" /etc/hosts"
                                   .format(akr_host))
        if AKRVariables.akr_platform_distro == 'debian':
            AKRShellExec.cmd_exec(self, "/etc/init.d/hostname.sh start")
        else:
            AKRShellExec.cmd_exec(self, "service hostname restart")

    else:
        akr_host = input("Enter hostname [fqdn]:")
        check_fqdn(self, akr_host)
