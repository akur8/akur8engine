"""Akur8Engine core variable module"""
import platform
import socket
import configparser
import os
import sys
import psutil
import datetime


class AKRVariables():
    """Intialization of core variables"""

    # Akur8Engine version
    akr_version = "3.4.1"


    # Akur8Engine packages versions
    akr_ik_cli = "0.22.0"
    akr_adminer = "4.2.1"
    akr_roundcube = "1.1.4"
    akr_vimbadmin = "3.0.12"

    # Get IKCLI path
    akr_ikcli_path = os.popen('which ik | tr "\n" " "').read()
    if akr_ikcli_path == '':
        akr_ikcli_path = '/usr/bin/ik '

    # Current date and time of System
    akr_date = datetime.datetime.now().strftime('%d%b%Y%H%M%S')

    # Akur8Engine core variables
    akr_platform_distro = platform.linux_distribution()[0].lower()
    akr_platform_version = platform.linux_distribution()[1]
    akr_platform_codename = os.popen("lsb_release -sc | tr -d \'\\n\'").read()

    # Get timezone of system
    if os.path.isfile('/etc/timezone'):
        with open("/etc/timezone", "r") as tzfile:
            akr_timezone = tzfile.read().replace('\n', '')
            if akr_timezone == "Etc/UTC":
                akr_timezone = "UTC"
    else:
        akr_timezone = "UTC"

    # Get FQDN of system
    akr_fqdn = socket.getfqdn()

    # EasyEngien default webroot path
    akr_webroot = '/var/www/'

    # PHP5 user
    akr_php_user = 'www-data'

    # Get git user name and EMail
    config = configparser.ConfigParser()
    config.read(os.path.expanduser("~")+'/.gitconfig')
    try:
        akr_user = config['user']['name']
        akr_email = config['user']['email']
    except Exception as e:
        akr_user = input("Enter your name: ")
        akr_email = input("Enter your email: ")
        os.system("git config --global user.name {0}".format(akr_user))
        os.system("git config --global user.email {0}".format(akr_email))

    # Get System RAM and SWAP details
    akr_ram = psutil.virtual_memory().total / (1024 * 1024)
    akr_swap = psutil.swap_memory().total / (1024 * 1024)

    # MySQL hostname
    akr_mysql_host = ""
    config = configparser.RawConfigParser()
    if os.path.exists('/etc/mysql/conf.d/my.cnf'):
      cnfpath = "/etc/mysql/conf.d/my.cnf"
    else:
      cnfpath = os.path.expanduser("~")+"/.my.cnf"
    if [cnfpath] == config.read(cnfpath):
        try:
            akr_mysql_host = config.get('client', 'host')
        except configparser.NoOptionError as e:
            akr_mysql_host = "localhost"
    else:
        akr_mysql_host = "localhost"

    # Akur8Engine stack installation variables
    # Nginx repo and packages
    if akr_platform_codename == 'precise':
        akr_nginx_repo = ("deb http://download.opensuse.org/repositories/home:"
                         "/rtCamp:/Akur8Engine/xUbuntu_12.04/ /")
        akr_nginx_dev_repo = ("deb http://download.opensuse.org/repositories/home:"
                             "/rtCamp:/Akur8Engine-dev/xUbuntu_12.04/ /")
    elif akr_platform_codename == 'trusty':
        akr_nginx_repo = ("deb http://download.opensuse.org/repositories/home:"
                         "/rtCamp:/Akur8Engine/xUbuntu_14.04/ /")
        akr_nginx_dev_repo = ("deb http://download.opensuse.org/repositories/home:"
                             "/rtCamp:/Akur8Engine-dev/xUbuntu_14.04/ /")
    elif akr_platform_codename == 'wheezy':
        akr_nginx_repo = ("deb http://download.opensuse.org/repositories/home:"
                         "/rtCamp:/Akur8Engine/Debian_7.0/ /")
        akr_nginx_dev_repo = None
    elif akr_platform_codename == 'jessie':
        akr_nginx_repo = ("deb http://download.opensuse.org/repositories/home:"
                         "/rtCamp:/Akur8Engine/Debian_8.0/ /")
        akr_nginx_dev_repo = ("deb http://download.opensuse.org/repositories/home:"
                             "/rtCamp:/Akur8Engine-dev/Debian_8.0/ /")


    akr_nginx = ["nginx-custom", "nginx-common"]
    akr_nginx_dev = ["nginx-mainline", "nginx-common"]
    akr_nginx_key = '3050AC3CD2AE6F03'

    # PHP repo and packages
    if akr_platform_distro == 'ubuntu':
        akr_php_repo = "ppa:ondrej/php5-5.6"
    elif akr_platform_codename == 'wheezy':
        akr_php_repo = ("deb http://packages.dotdeb.org {codename}-php56 all"
                       .format(codename=akr_platform_codename))
    akr_php = ["php5-fpm", "php5-curl", "php5-gd", "php5-imap",
              "php5-mcrypt", "php5-common", "php5-readline",
              "php5-mysql", "php5-cli", "php5-memcache", "php5-imagick",
              "memcached", "graphviz", "php-pear"]

    if akr_platform_codename == 'wheezy':
        akr_php = akr_php + ["php5-dev"]

    if akr_platform_distro == 'ubuntu' or akr_platform_codename == 'jessie':
        akr_php = akr_php + ["php5-xdebug"]

    # MySQL repo and packages
    if akr_platform_distro == 'ubuntu':
        akr_mysql_repo = ("deb http://sfo1.mirrors.digitalocean.com/mariadb/repo/"
                         "10.1/ubuntu {codename} main"
                         .format(codename=akr_platform_codename))
    elif akr_platform_distro == 'debian':
        akr_mysql_repo = ("deb http://sfo1.mirrors.digitalocean.com/mariadb/repo/"
                         "10.1/debian {codename} main"
                         .format(codename=akr_platform_codename))

    akr_mysql = ["mariadb-server", "percona-toolkit"]

    # Postfix repo and packages
    akr_postfix_repo = ""
    akr_postfix = ["postfix"]

    # Mail repo and packages
    akr_mail_repo = ("deb http://http.debian.net/debian-backports {codename}"
                    "-backports main".format(codename=akr_platform_codename))

    akr_mail = ["dovecot-core", "dovecot-imapd", "dovecot-pop3d",
               "dovecot-lmtpd", "dovecot-mysql", "dovecot-sieve",
               "dovecot-managesieved", "postfix-mysql", "php5-cgi",
               "php-gettext", "php-pear"]

    # Mailscanner repo and packages
    akr_mailscanner_repo = ()
    akr_mailscanner = ["amavisd-new", "spamassassin", "clamav", "clamav-daemon",
                      "arj", "zoo", "nomarch", "lzop", "cabextract", "p7zip",
                      "rpm", "unrar-free"]

    # HHVM repo details
    # 12.04 requires boot repository
    if akr_platform_distro == 'ubuntu':
        if akr_platform_codename == "precise":
            akr_boost_repo = ("ppa:mapnik/boost")

        akr_hhvm_repo = ("deb http://dl.hhvm.com/ubuntu {codename} main"
                        .format(codename=akr_platform_codename))
    else:
        akr_hhvm_repo = ("deb http://dl.hhvm.com/debian {codename} main"
                        .format(codename=akr_platform_codename))

    akr_hhvm = ["hhvm"]

    # Redis repo details
    if akr_platform_distro == 'ubuntu':
        akr_redis_repo = ("ppa:chris-lea/redis-server")

    else:
        akr_redis_repo = ("deb http://packages.dotdeb.org {codename} all"
                        .format(codename=akr_platform_codename))

    akr_redis = ['redis-server', 'php5-redis']

    # Repo path
    akr_repo_file = "akr-repo.list"
    akr_repo_file_path = ("/etc/apt/sources.list.d/" + akr_repo_file)

    # Application dabase file path
    basedir = os.path.abspath(os.path.dirname('/var/lib/akr/'))
    akr_db_uri = 'sqlite:///' + os.path.join(basedir, 'akr.db')

    def __init__(self):
        pass
