"""Akur8Engine exception classes."""


class AKRError(Exception):
    """Generic errors."""
    def __init__(self, msg):
        Exception.__init__(self)
        self.msg = msg

    def __str__(self):
        return self.msg


class AKRConfigError(AKRError):
    """Config related errors."""
    pass


class AKRRuntimeError(AKRError):
    """Generic runtime errors."""
    pass


class AKRArgumentError(AKRError):
    """Argument related errors."""
    pass
