"""Akur8Engine SWAP creation"""
from akr.core.variables import AKRVariables
from akr.core.shellexec import AKRShellExec
from akr.core.fileutils import AKRFileUtils
from akr.core.aptget import AKRAptGet
from akr.core.logging import Log
import os


class AKRSwap():
    """Manage Swap"""

    def __init__():
        """Initialize """
        pass

    def add(self):
        """Swap addition with Akur8Engine"""
        if AKRVariables.akr_ram < 512:
            if AKRVariables.akr_swap < 1000:
                Log.info(self, "Adding SWAP file, please wait...")

                # Install dphys-swapfile
                AKRAptGet.update(self)
                AKRAptGet.install(self, ["dphys-swapfile"])
                # Stop service
                AKRShellExec.cmd_exec(self, "service dphys-swapfile stop")
                # Remove Default swap created
                AKRShellExec.cmd_exec(self, "/sbin/dphys-swapfile uninstall")

                # Modify Swap configuration
                if os.path.isfile("/etc/dphys-swapfile"):
                    AKRFileUtils.searchreplace(self, "/etc/dphys-swapfile",
                                              "#CONF_SWAPFILE=/var/swap",
                                              "CONF_SWAPFILE=/akr-swapfile")
                    AKRFileUtils.searchreplace(self,  "/etc/dphys-swapfile",
                                              "#CONF_MAXSWAP=2048",
                                              "CONF_MAXSWAP=1024")
                    AKRFileUtils.searchreplace(self,  "/etc/dphys-swapfile",
                                              "#CONF_SWAPSIZE=",
                                              "CONF_SWAPSIZE=1024")
                else:
                    with open("/etc/dphys-swapfile", 'w') as conffile:
                        conffile.write("CONF_SWAPFILE=/akr-swapfile\n"
                                       "CONF_SWAPSIZE=1024\n"
                                       "CONF_MAXSWAP=1024\n")
                # Create swap file
                AKRShellExec.cmd_exec(self, "service dphys-swapfile start")
