"""Akur8Engine bootstrapping."""

# All built-in application controllers should be imported, and registered
# in this file in the same way as AKRBaseController.

from cement.core import handler
from akr.cli.controllers.base import AKRBaseController


def load(app):
    handler.register(AKRBaseController)
