from cement.core.controller import CementBaseController, expose
from cement.core import handler, hook
from akr.core.mysql import AKRMysql
from akr.core.logging import Log
from akr.core.variables import AKRVariables
from akr.core.aptget import AKRAptGet
from akr.core.shellexec import AKRShellExec
from akr.core.apt_repo import AKRRepo
from akr.core.services import AKRService
import configparser
import os


class AKRStackMigrateController(CementBaseController):
    class Meta:
        label = 'migrate'
        stacked_on = 'stack'
        stacked_type = 'nested'
        description = ('Migrate stack safely')
        arguments = [
            (['--mariadb'],
                dict(help="Migrate database to MariaDB",
                     action='store_true')),
            # (['--PHP'],
            #    dict(help="update to html site", action='store_true')),
            ]

    @expose(hide=True)
    def migrate_mariadb(self):
        # Backup all database
        AKRMysql.backupAll(self)

        # Add MariaDB repo
        Log.info(self, "Adding repository for MariaDB, please wait...")

        mysql_pref = ("Package: *\nPin: origin sfo1.mirrors.digitalocean.com"
                      "\nPin-Priority: 1000\n")
        with open('/etc/apt/preferences.d/'
                  'MariaDB.pref', 'w') as mysql_pref_file:
            mysql_pref_file.write(mysql_pref)

        AKRRepo.add(self, repo_url=AKRVariables.akr_mysql_repo)
        Log.debug(self, 'Adding key for {0}'
                  .format(AKRVariables.akr_mysql_repo))
        AKRRepo.add_key(self, '0xcbcb082a1bb943db',
                       keyserver="keyserver.ubuntu.com")

        config = configparser.ConfigParser()
        if os.path.exists('/etc/mysql/conf.d/my.cnf'):
            config.read('/etc/mysql/conf.d/my.cnf')
        else:
            config.read(os.path.expanduser("~")+'/.my.cnf')

        try:
            chars = config['client']['password']
        except Exception as e:
            Log.error(self, "Error: process exited with error %s"
                            % e)

        Log.debug(self, "Pre-seeding MariaDB")
        Log.debug(self, "echo \"mariadb-server-10.0 "
                        "mysql-server/root_password "
                        "password \" | "
                        "debconf-set-selections")
        AKRShellExec.cmd_exec(self, "echo \"mariadb-server-10.0 "
                                   "mysql-server/root_password "
                                   "password {chars}\" | "
                                   "debconf-set-selections"
                                   .format(chars=chars),
                                   log=False)
        Log.debug(self, "echo \"mariadb-server-10.0 "
                        "mysql-server/root_password_again "
                        "password \" | "
                        "debconf-set-selections")
        AKRShellExec.cmd_exec(self, "echo \"mariadb-server-10.0 "
                                   "mysql-server/root_password_again "
                                   "password {chars}\" | "
                                   "debconf-set-selections"
                                   .format(chars=chars),
                                   log=False)

        # Install MariaDB
        apt_packages = AKRVariables.akr_mysql

        # If PHP is installed then install php5-mysql
        if AKRAptGet.is_installed(self, "php5-fpm"):
            apt_packages = apt_packages + ["php5-mysql"]

        # If mail server is installed then install dovecot-sql and postfix-sql
        if AKRAptGet.is_installed(self, "dovecot-core"):
            apt_packages = apt_packages + ["dovecot-mysql", "postfix-mysql",
                                           "libclass-dbi-mysql-perl"]

        Log.info(self, "Updating apt-cache, please wait...")
        AKRAptGet.update(self)
        Log.info(self, "Installing MariaDB, please wait...")
        AKRAptGet.remove(self, ["mysql-common", "libmysqlclient18"])
        AKRAptGet.auto_remove(self)
        AKRAptGet.install(self, apt_packages)

        # Restart  dovecot and postfix if installed
        if AKRAptGet.is_installed(self, "dovecot-core"):
            AKRService.restart_service(self, 'dovecot')
            AKRService.restart_service(self, 'postfix')

    @expose(hide=True)
    def default(self):
        if ((not self.app.pargs.mariadb)):
            self.app.args.print_help()
        if self.app.pargs.mariadb:
            if AKRVariables.akr_mysql_host is not "localhost":
                Log.error(self, "Remote MySQL found, Akur8Engine will not "
                          "install MariaDB")

            if AKRShellExec.cmd_exec(self, "mysqladmin ping") and (not
               AKRAptGet.is_installed(self, 'mariadb-server')):

                Log.info(self, "If your database size is big, "
                         "migration may take some time.")
                Log.info(self, "During migration non nginx-cached parts of "
                         "your site may remain down")
                start_migrate = input("Type \"mariadb\" to continue:")
                if start_migrate != "mariadb":
                    Log.error(self, "Not starting migration")
                self.migrate_mariadb()
            else:
                Log.error(self, "Your current MySQL is not alive or "
                          "you allready installed MariaDB")
