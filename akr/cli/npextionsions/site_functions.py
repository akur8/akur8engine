from akr.cli.npextionsions.stack import AKRStackController
from akr.core.fileutils import AKRFileUtils
from akr.core.mysql import *
from akr.core.shellexec import *
from akr.core.sslutils import SSL
from akr.core.variables import AKRVariables
from akr.cli.npextionsions.sitedb import *
from akr.core.aptget import AKRAptGet
from akr.core.git import AKRGit
from akr.core.logging import Log
from akr.core.sendmail import AKRSendMail
from akr.core.services import AKRService
import subprocess
from subprocess import CalledProcessError
import os
import random
import string
import sys
import getpass
import glob
import re
import platform


class SiteError(Exception):
    """Custom Exception Occured when setting up site"""
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


def pre_run_checks(self):

    # Check nginx configuration
    Log.info(self, "Running pre-update checks, please wait...")
    try:
        Log.debug(self, "checking NGINX configuration ...")
        FNULL = open('/dev/null', 'w')
        ret = subprocess.check_call(["nginx", "-t"], stdout=FNULL,
                                    stderr=subprocess.STDOUT)
    except CalledProcessError as e:
        Log.debug(self, "{0}".format(str(e)))
        raise SiteError("nginx configuration check failed.")


def check_domain_exists(self, domain):
    if getSiteInfo(self, domain):
        return True
    else:
        return False


def setupdomain(self, data):

    akr_domain_name = data['site_name']
    akr_site_webroot = data['webroot'] if 'webroot' in data.keys() else ''

    # Check if nginx configuration already exists
    # if os.path.isfile('/etc/nginx/sites-available/{0}'
    #                   .format(akr_domain_name)):
    #     raise SiteError("nginx configuration already exists for site")

    Log.info(self, "Setting up NGINX configuration \t", end='')
    # write nginx config for file
    try:
        akr_site_nginx_conf = open('/etc/nginx/sites-available/{0}'
                                  .format(akr_domain_name), encoding='utf-8',
                                  mode='w')

        self.app.render((data), 'virtualconf.mustache',
                        out=akr_site_nginx_conf)
        akr_site_nginx_conf.close()
    except IOError as e:
        Log.debug(self, "{0}".format(e))
        raise SiteError("create nginx configuration failed for site")
    except Exception as e:
        Log.debug(self, "{0}".format(e))
        raise SiteError("create nginx configuration failed for site")
    finally:
        # Check nginx -t and return status over it
        try:
            Log.debug(self, "Checking generated nginx conf, please wait...")
            FNULL = open('/dev/null', 'w')
            ret = subprocess.check_call(["nginx", "-t"], stdout=FNULL,
                                        stderr=subprocess.STDOUT)
            Log.info(self, "[" + Log.ENDC + "Done" + Log.OKBLUE + "]")
        except CalledProcessError as e:
            Log.debug(self, "{0}".format(str(e)))
            Log.info(self, "[" + Log.ENDC + Log.FAIL + "Fail"
                     + Log.OKBLUE + "]")
            raise SiteError("created nginx configuration failed for site."
                            " check with `nginx -t`")


    # create symbolic link for
    AKRFileUtils.create_symlink(self, ['/etc/nginx/sites-available/{0}'
                                      .format(akr_domain_name),
                                      '/etc/nginx/sites-enabled/{0}'
                                      .format(akr_domain_name)])

    if 'proxy' in data.keys() and data['proxy']:
        return

    # Creating htdocs & logs directory
    Log.info(self, "Setting up webroot \t\t", end='')
    try:
        if not os.path.exists('{0}/htdocs'.format(akr_site_webroot)):
            os.makedirs('{0}/htdocs'.format(akr_site_webroot))
        if not os.path.exists('{0}/logs'.format(akr_site_webroot)):
            os.makedirs('{0}/logs'.format(akr_site_webroot))
        if not os.path.exists('{0}/conf/nginx'.format(akr_site_webroot)):
            os.makedirs('{0}/conf/nginx'.format(akr_site_webroot))

        AKRFileUtils.create_symlink(self, ['/var/log/nginx/{0}.access.log'
                                          .format(akr_domain_name),
                                          '{0}/logs/access.log'
                                          .format(akr_site_webroot)])
        AKRFileUtils.create_symlink(self, ['/var/log/nginx/{0}.error.log'
                                          .format(akr_domain_name),
                                          '{0}/logs/error.log'
                                          .format(akr_site_webroot)])
    except Exception as e:
        Log.debug(self, "{0}".format(e))
        raise SiteError("setup webroot failed for site")
    finally:
        # TODO Check if directories are setup
        if (os.path.exists('{0}/htdocs'.format(akr_site_webroot)) and
           os.path.exists('{0}/logs'.format(akr_site_webroot))):
            Log.info(self, "[" + Log.ENDC + "Done" + Log.OKBLUE + "]")
        else:
            Log.info(self, "[" + Log.ENDC + "Fail" + Log.OKBLUE + "]")
            raise SiteError("setup webroot failed for site")


def setupdatabase(self, data):
    akr_domain_name = data['site_name']
    akr_random = (''.join(random.sample(string.ascii_uppercase +
                 string.ascii_lowercase + string.digits, 15)))
    akr_replace_dot = akr_domain_name.replace('.', '_')
    prompt_dbname = self.app.config.get('mysql', 'db-name')
    prompt_dbuser = self.app.config.get('mysql', 'db-user')
    akr_mysql_grant_host = self.app.config.get('mysql', 'grant-host')
    akr_db_name = ''
    akr_db_username = ''
    akr_db_password = ''

    if prompt_dbname == 'True' or prompt_dbname == 'true':
        try:
            akr_db_name = input('Enter the MySQL database name [{0}]: '
                               .format(akr_replace_dot))
        except EOFError as e:
            Log.debug(self, "{0}".format(e))
            raise SiteError("Unable to input database name")

    if not akr_db_name:
        akr_db_name = akr_replace_dot

    if prompt_dbuser == 'True' or prompt_dbuser == 'true':
        try:
            akr_db_username = input('Enter the MySQL database user name [{0}]: '
                                   .format(akr_replace_dot))
            akr_db_password = getpass.getpass(prompt='Enter the MySQL database'
                                             ' password [{0}]: '
                                             .format(akr_random))
        except EOFError as e:
            Log.debug(self, "{0}".format(e))
            raise SiteError("Unable to input database credentials")

    if not akr_db_username:
        akr_db_username = akr_replace_dot
    if not akr_db_password:
        akr_db_password = akr_random

    if len(akr_db_username) > 16:
        Log.debug(self, 'Autofix MySQL username (ERROR 1470 (HY000)),'
                  ' please wait')
        akr_db_username = (akr_db_name[0:6] + generate_random())

    # create MySQL database
    Log.info(self, "Setting up database\t\t", end='')
    Log.debug(self, "Creating database {0}".format(akr_db_name))
    try:
        if AKRMysql.check_db_exists(self, akr_db_name):
            Log.debug(self, "Database already exists, Updating DB_NAME .. ")
            akr_db_name = (akr_db_name[0:6] + generate_random())
            akr_db_username = (akr_db_name[0:6] + generate_random())
    except MySQLConnectionError as e:
        raise SiteError("MySQL Connectivity problem occured")

    try:
        AKRMysql.execute(self, "create database `{0}`"
                        .format(akr_db_name))
    except StatementExcecutionError as e:
        Log.info(self, "[" + Log.ENDC + Log.FAIL + "Failed" + Log.OKBLUE + "]")
        raise SiteError("create database execution failed")
    # Create MySQL User
    Log.debug(self, "Creating user {0}".format(akr_db_username))
    Log.debug(self, "create user `{0}`@`{1}` identified by ''"
              .format(akr_db_username, akr_mysql_grant_host))
    try:
        AKRMysql.execute(self,
                        "create user `{0}`@`{1}` identified by '{2}'"
                        .format(akr_db_username, akr_mysql_grant_host,
                                akr_db_password), log=False)
    except StatementExcecutionError as e:
        Log.info(self, "[" + Log.ENDC + Log.FAIL + "Failed" + Log.OKBLUE + "]")
        raise SiteError("creating user failed for database")

    # Grant permission
    Log.debug(self, "Setting up user privileges")
    try:
        AKRMysql.execute(self,
                        "grant all privileges on `{0}`.* to `{1}`@`{2}`"
                        .format(akr_db_name,
                                akr_db_username, akr_mysql_grant_host))
    except StatementExcecutionError as e:
        Log.info(self, "[" + Log.ENDC + Log.FAIL + "Failed" + Log.OKBLUE + "]")
        SiteError("grant privileges to user failed for database ")

    Log.info(self, "[" + Log.ENDC + "Done" + Log.OKBLUE + "]")

    data['akr_db_name'] = akr_db_name
    data['akr_db_user'] = akr_db_username
    data['akr_db_pass'] = akr_db_password
    data['akr_db_host'] = AKRVariables.akr_mysql_host
    data['akr_mysql_grant_host'] = akr_mysql_grant_host
    return(data)


def setupnanopublishr(self, data):
    akr_domain_name = data['site_name']
    akr_site_webroot = data['webroot']
    prompt_ikprefix = self.app.config.get('nanopublishr', 'prefix')
    akr_ik_user = self.app.config.get('nanopublishr', 'user')
    akr_ik_pass = self.app.config.get('nanopublishr', 'password')
    akr_ik_email = self.app.config.get('nanopublishr', 'email')
    # Random characters
    akr_random = (''.join(random.sample(string.ascii_uppercase +
                 string.ascii_lowercase + string.digits, 15)))
    akr_ik_prefix = ''
    # akr_ik_user = ''
    # akr_ik_pass = ''

    if 'ik-user' in data.keys() and data['ik-user']:
        akr_ik_user = data['ik-user']
    if 'ik-email' in data.keys() and data['ik-email']:
        akr_ik_email = data['ik-email']
    if 'ik-pass' in data.keys() and data['ik-pass']:
        akr_ik_pass = data['ik-pass']

    Log.info(self, "Downloading NanoPublisher \t\t", end='')
    AKRFileUtils.chdir(self, '{0}/htdocs/'.format(akr_site_webroot))
    try:
        if AKRShellExec.cmd_exec(self, "ik --allow-root core"
                             " download"):
            pass
        else:
            Log.info(self, "[" + Log.ENDC + Log.FAIL + "Fail" + Log.OKBLUE + "]")
            raise SiteError("download NanoPublisher core failed")
    except CommandExecutionError as e:
        Log.info(self, "[" + Log.ENDC + Log.FAIL + "Fail" + Log.OKBLUE + "]")
        raise SiteError(self, "download NanoPublisher core failed")

    Log.info(self, "[" + Log.ENDC + "Done" + Log.OKBLUE + "]")

    if not (data['akr_db_name'] and data['akr_db_user'] and data['akr_db_pass']):
        data = setupdatabase(self, data)
    if prompt_ikprefix == 'True' or prompt_ikprefix == 'true':
        try:
            akr_ik_prefix = input('Enter the NanoPublisher table prefix [ik_]: ')
            while not re.match('^[A-Za-z0-9_]*$', akr_ik_prefix):
                Log.warn(self, "table prefix can only "
                         "contain numbers, letters, and underscores")
                akr_ik_prefix = input('Enter the NanoPublisher table prefix [ik_]: '
                                     )
        except EOFError as e:
            Log.debug(self, "{0}".format(e))
            raise SiteError("input table prefix failed")

    if not akr_ik_prefix:
        akr_ik_prefix = 'ik_'

    # Modify np-konfig.php & move outside the webroot

    AKRFileUtils.chdir(self, '{0}/htdocs/'.format(akr_site_webroot))
    Log.debug(self, "Setting up np-konfig file")
    if not data['multisite']:
        Log.debug(self, "Generating np-konfig for NanoPublisher Single site")
        Log.debug(self, "bash -c \"php {0} --allow-root "
                  .format(AKRVariables.akr_ikcli_path)
                  + "core config "
                  + "--dbname=\'{0}\' --dbprefix=\'{1}\' --dbuser=\'{2}\' "
                  "--dbhost=\'{3}\' "
                  .format(data['akr_db_name'], akr_ik_prefix,
                          data['akr_db_user'], data['akr_db_host'])
                  + "--dbpass= "
                  "--extra-php<<PHP \n {1}\nPHP\""
                  .format(data['akr_db_pass'],
                          "\n\ndefine(\'IK_DEBUG\', false);"))
        try:
            if AKRShellExec.cmd_exec(self, "bash -c \"php {0} --allow-root"
                                 .format(AKRVariables.akr_ikcli_path)
                                 + " core config "
                                 + "--dbname=\'{0}\' --dbprefix=\'{1}\' "
                                 "--dbuser=\'{2}\' --dbhost=\'{3}\' "
                                 .format(data['akr_db_name'], akr_ik_prefix,
                                         data['akr_db_user'], data['akr_db_host']
                                         )
                                 + "--dbpass=\'{0}\' "
                                   "--extra-php<<PHP \n {1} {redissalt}\nPHP\""
                                   .format(data['akr_db_pass'],
                                           "\n\ndefine(\'IK_DEBUG\', false);",
                                           redissalt="\n\ndefine( \'IK_CACHE_KEY_SALT\', \'{0}:\' );"
                                                      .format(akr_domain_name) if data['ikredis']
                                                      else ''),
                                   log=False
                                 ):
                pass
            else :
                raise SiteError("generate np-konfig failed for ik single site")
        except CommandExecutionError as e:
                raise SiteError("generate np-konfig failed for ik single site")
    else:
        Log.debug(self, "Generating np-konfig for NanoPublisher multisite")
        Log.debug(self, "bash -c \"php {0} --allow-root "
                  .format(AKRVariables.akr_ikcli_path)
                  + "core config "
                  + "--dbname=\'{0}\' --dbprefix=\'{1}\' --dbhost=\'{2}\' "
                  .format(data['akr_db_name'], akr_ik_prefix, data['akr_db_host'])
                  + "--dbuser=\'{0}\' --dbpass= "
                  "--extra-php<<PHP \n {2} {3} {4}\nPHP\""
                  .format(data['akr_db_user'], data['akr_db_pass'],
                          "\ndefine(\'IK_ALLOW_MULTISITE\', "
                          "true);",
                          "\ndefine(\'IKNTWRK_ACCEL_REDIRECT\',"
                          " true);",
                          "\n\ndefine(\'IK_DEBUG\', false);"))
        try:
            if AKRShellExec.cmd_exec(self, "bash -c \"php {0} --allow-root"
                                 .format(AKRVariables.akr_ikcli_path)
                                 + " core config "
                                 + "--dbname=\'{0}\' --dbprefix=\'{1}\' "
                                 "--dbhost=\'{2}\' "
                                 .format(data['akr_db_name'], akr_ik_prefix,
                                         data['akr_db_host'])
                                 + "--dbuser=\'{0}\' --dbpass=\'{1}\' "
                                   "--extra-php<<PHP \n {2} {3} {4} {redissalt}\nPHP\""
                                 .format(data['akr_db_user'],
                                         data['akr_db_pass'],
                                         "\ndefine(\'IK_ALLOW_MULTISITE\', "
                                         "true);",
                                         "\ndefine(\'IKNTWRK_ACCEL_REDIRECT\',"
                                         " true);",
                                         "\n\ndefine(\'IK_DEBUG\', false);",
                                         redissalt="\n\ndefine( \'IK_CACHE_KEY_SALT\', \'{0}:\' );"
                                                      .format(akr_domain_name) if data['ikredis']
                                                      else ''),
                                 log=False
                                 ):
                pass
            else:
                raise SiteError("generate np-konfig failed for ik multi site")
        except CommandExecutionError as e:
                raise SiteError("generate np-konfig failed for ik multi site")

    #AKRFileUtils.mvfile(self, os.getcwd()+'/np-konfig.php',
    #                   os.path.abspath(os.path.join(os.getcwd(), os.pardir)))

    try:
        import shutil

        Log.debug(self, "Moving file from {0} to {1}".format(os.getcwd()+'/np-konfig.php',os.path.abspath(os.path.join(os.getcwd(), os.pardir))))
        shutil.move(os.getcwd()+'/np-konfig.php',os.path.abspath(os.path.join(os.getcwd(), os.pardir)))
    except Exception as e:
        Log.error(self, 'Unable to move file from {0} to {1}'
                      .format(os.getcwd()+'/np-konfig.php', os.path.abspath(os.path.join(os.getcwd(), os.pardir))),False)
        raise SiteError("Unable to move np-konfig.php")


    if not akr_ik_user:
        akr_ik_user = AKRVariables.akr_user
        while not akr_ik_user:
            Log.warn(self, "Username can have only alphanumeric"
                     "characters, spaces, underscores, hyphens,"
                     "periods and the @ symbol.")
            try:
                akr_ik_user = input('Enter NanoPublisher username: ')
            except EOFError as e:
                Log.debug(self, "{0}".format(e))
                raise SiteError("input NanoPublisher username failed")
    if not akr_ik_pass:
        akr_ik_pass = akr_random

    if not akr_ik_email:
        akr_ik_email = AKRVariables.akr_email
        while not akr_ik_email:
            try:
                akr_ik_email = input('Enter NanoPublisher email: ')
            except EOFError as e:
                Log.debug(self, "{0}".format(e))
                raise SiteError("input NanoPublisher username failed")

    try:
        while not re.match(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$",
                           akr_ik_email):
            Log.info(self, "EMail not Valid in config, "
                     "Please provide valid email id")
            akr_ik_email = input("Enter your email: ")
    except EOFError as e:
        Log.debug(self, "{0}".format(e))
        raise SiteError("input NanoPublisher user email failed")

    Log.debug(self, "Setting up NanoPublisher tables")

    if not data['multisite']:
        Log.debug(self, "Creating tables for NanoPublisher Single site")
        Log.debug(self, "php {0} --allow-root core install "
                  .format(AKRVariables.akr_ikcli_path)
                  + "--url=\'{0}\' --title=\'{0}\' --admin_name=\'{1}\' "
                  .format(data['www_domain'], akr_ik_user)
                  + "--admin_password= --admin_email=\'{1}\'"
                  .format(akr_ik_pass, akr_ik_email))
        try:
            if AKRShellExec.cmd_exec(self, "php {0} --allow-root core "
                                 .format(AKRVariables.akr_ikcli_path)
                                 + "install --url=\'{0}\' --title=\'{0}\' "
                                 "--admin_name=\'{1}\' "
                                 .format(data['www_domain'], akr_ik_user)
                                 + "--admin_password=\'{0}\' "
                                 "--admin_email=\'{1}\'"
                                 .format(akr_ik_pass, akr_ik_email),
                                 log=False):
                pass
            else:
                raise SiteError("setup NanoPublisher tables failed for single site")
        except CommandExecutionError as e:
            raise SiteError("setup NanoPublisher tables failed for single site")
    else:
        Log.debug(self, "Creating tables for NanoPublisher multisite")
        Log.debug(self, "php {0} --allow-root "
                  .format(AKRVariables.akr_ikcli_path)
                  + "core multisite-install "
                  "--url=\'{0}\' --title=\'{0}\' --admin_name=\'{1}\' "
                  .format(data['www_domain'], akr_ik_user)
                  + "--admin_password= --admin_email=\'{1}\' "
                  "{subdomains}"
                  .format(akr_ik_pass, akr_ik_email,
                          subdomains='--subdomains'
                          if not data['iksubdir'] else ''))
        try:
            if AKRShellExec.cmd_exec(self, "php {0} --allow-root "
                                 .format(AKRVariables.akr_ikcli_path)
                                 + "core multisite-install "
                                 "--url=\'{0}\' --title=\'{0}\' "
                                 "--admin_name=\'{1}\' "
                                 .format(data['www_domain'], akr_ik_user)
                                 + "--admin_password=\'{0}\' "
                                 "--admin_email=\'{1}\' "
                                 "{subdomains}"
                                 .format(akr_ik_pass, akr_ik_email,
                                         subdomains='--subdomains'
                                         if not data['iksubdir'] else ''),
                                 log=False):
                pass
            else:
                raise SiteError("setup NanoPublisher tables failed for ik multi site")
        except CommandExecutionError as e:
            raise SiteError("setup NanoPublisher tables failed for ik multi site")

    Log.debug(self, "Updating NanoPublisher permalink")
    try:
        AKRShellExec.cmd_exec(self, " php {0} --allow-root "
                             .format(AKRVariables.akr_ikcli_path)
                             + "rewrite structure "
                             "/%year%/%monthnum%/%day%/%postname%/")
    except CommandExecutionError as e:
        raise SiteError("Update nanopublishr permalinks failed")

    """Install nginx-helper npextionsion """
    installik_npextionsion(self, 'nginx-helper', data)
    if data['ikfc']:
        npextionsion_data = '{"log_level":"INFO","log_filesize":5,"enable_purge":1,"enable_map":0,"enable_log":0,"enable_stamp":0,"purge_homepage_on_new":1,"purge_homepage_on_edit":1,"purge_homepage_on_del":1,"purge_archive_on_new":1,"purge_archive_on_edit":0,"purge_archive_on_del":0,"purge_archive_on_new_comment":0,"purge_archive_on_deleted_comment":0,"purge_page_on_mod":1,"purge_page_on_new_comment":1,"purge_page_on_deleted_comment":1,"cache_method":"enable_fastcgi","purge_method":"get_request","redis_hostname":"127.0.0.1","redis_port":"6379","redis_prefix":"nginx-cache:"}'
        setupik_npextionsion(self, 'nginx-helper', 'rt_ik_nginx_helper_options', npextionsion_data, data)
    elif data['ikredis']:
        npextionsion_data = '{"log_level":"INFO","log_filesize":5,"enable_purge":1,"enable_map":0,"enable_log":0,"enable_stamp":0,"purge_homepage_on_new":1,"purge_homepage_on_edit":1,"purge_homepage_on_del":1,"purge_archive_on_new":1,"purge_archive_on_edit":0,"purge_archive_on_del":0,"purge_archive_on_new_comment":0,"purge_archive_on_deleted_comment":0,"purge_page_on_mod":1,"purge_page_on_new_comment":1,"purge_page_on_deleted_comment":1,"cache_method":"enable_redis","purge_method":"get_request","redis_hostname":"127.0.0.1","redis_port":"6379","redis_prefix":"nginx-cache:"}'
        setupik_npextionsion(self, 'nginx-helper', 'rt_ik_nginx_helper_options', npextionsion_data, data)

    """Install Ik Super Cache"""
    if data['iksc']:
        installik_npextionsion(self, 'ik-super-cache', data)

    """Install Redis Cache"""
    if data['ikredis']:
        installik_npextionsion(self, 'redis-cache', data)

    """Install W3 Total Cache"""
    if data['w3tc'] or data['ikfc']:
        installik_npextionsion(self, 'w3-total-cache', data)

    ik_creds = dict(ik_user=akr_ik_user, ik_pass=akr_ik_pass,
                    ik_email=akr_ik_email)

    return(ik_creds)


def setupnanopublishrnetwork(self, data):
    akr_site_webroot = data['webroot']
    AKRFileUtils.chdir(self, '{0}/htdocs/'.format(akr_site_webroot))
    Log.info(self, "Setting up NanoPublisher Network \t", end='')
    try:
        if AKRShellExec.cmd_exec(self, 'ik --allow-root core multisite-convert'
                             ' --title=\'{0}\' {subdomains}'
                             .format(data['www_domain'],
                                     subdomains='--subdomains'
                                     if not data['iksubdir'] else '')):
            pass
        else:
            Log.info(self, "[" + Log.ENDC + Log.FAIL + "Fail" + Log.OKBLUE + "]")
            raise SiteError("setup NanoPublisher network failed")

    except CommandExecutionError as e:
        Log.info(self, "[" + Log.ENDC + Log.FAIL + "Fail" + Log.OKBLUE + "]")
        raise SiteError("setup NanoPublisher network failed")
    Log.info(self, "[" + Log.ENDC + "Done" + Log.OKBLUE + "]")


def installik_npextionsion(self, npextionsion_name, data):
    akr_site_webroot = data['webroot']
    Log.info(self, "Installing npextionsion {0}, please wait..."
             .format(npextionsion_name))
    AKRFileUtils.chdir(self, '{0}/htdocs/'.format(akr_site_webroot))
    try:
        AKRShellExec.cmd_exec(self, "php {0} npextionsion "
                             .format(AKRVariables.akr_ikcli_path)
                             + "--allow-root install "
                             "{0}".format(npextionsion_name))
    except CommandExecutionError as e:
        raise SiteError("npextionsion installation failed")

    try:
        AKRShellExec.cmd_exec(self, "php {0} npextionsion "
                             .format(AKRVariables.akr_ikcli_path)
                             + "--allow-root activate "
                             "{0} {na}"
                             .format(npextionsion_name,
                                     na='--network' if data['multisite']
                                     else ''
                                     ))
    except CommandExecutionError as e:
        raise SiteError("npextionsion activation failed")

    return 1


def uninstallik_npextionsion(self, npextionsion_name, data):
    akr_site_webroot = data['webroot']
    Log.debug(self, "Uninstalling npextionsion {0}, please wait..."
              .format(npextionsion_name))
    AKRFileUtils.chdir(self, '{0}/htdocs/'.format(akr_site_webroot))
    Log.info(self, "Uninstalling npextionsion {0}, please wait..."
             .format(npextionsion_name))
    try:
        AKRShellExec.cmd_exec(self, "php {0} npextionsion "
                             .format(AKRVariables.akr_ikcli_path)
                             + "--allow-root deactivate "
                             "{0}".format(npextionsion_name))

        AKRShellExec.cmd_exec(self, "php {0} npextionsion "
                             .format(AKRVariables.akr_ikcli_path)
                             + "--allow-root uninstall "
                             "{0}".format(npextionsion_name))
    except CommandExecutionError as e:
        raise SiteError("npextionsion uninstall failed")

def setupik_npextionsion(self, npextionsion_name, npextionsion_option, npextionsion_data, data):
    akr_site_webroot = data['webroot']
    Log.info(self, "Setting npextionsion {0}, please wait..."
             .format(npextionsion_name))
    AKRFileUtils.chdir(self, '{0}/htdocs/'.format(akr_site_webroot))

    if not data['multisite']:
        try:
            AKRShellExec.cmd_exec(self, "php {0} "
                                 .format(AKRVariables.akr_ikcli_path)
                                 + "--allow-root option update "
                                 "{0} \'{1}\' --format=json".format(npextionsion_option, npextionsion_data))
        except CommandExecutionError as e:
            raise SiteError("npextionsion setup failed")
    else:
        try:
            AKRShellExec.cmd_exec(self, "php {0} "
                                 .format(AKRVariables.akr_ikcli_path)
                                 + "--allow-root network meta update 1 "
                                  "{0} \'{1}\' --format=json"
                                 .format(npextionsion_option, npextionsion_data
                                         ))
        except CommandExecutionError as e:
            raise SiteError("npextionsion setup failed")


def setwebrootpermissions(self, webroot):
    Log.debug(self, "Setting up permissions")
    try:
        AKRFileUtils.chown(self, webroot, AKRVariables.akr_php_user,
                          AKRVariables.akr_php_user, recursive=True)
    except Exception as e:
        Log.debug(self, str(e))
        raise SiteError("problem occured while setting up webroot permissions")


def sitebackup(self, data):
    akr_site_webroot = data['webroot']
    backup_path = akr_site_webroot + '/backup/{0}'.format(AKRVariables.akr_date)
    if not AKRFileUtils.isexist(self, backup_path):
        AKRFileUtils.mkdir(self, backup_path)
    Log.info(self, "Backup location : {0}".format(backup_path))
    AKRFileUtils.copyfile(self, '/etc/nginx/sites-available/{0}'
                         .format(data['site_name']), backup_path)

    if data['currsitetype'] in ['html', 'php', 'proxy', 'mysql']:
        if (data['pagespeed'] is True or data['old_pagespeed_status'] is True) and not data['ik']:
            Log.info(self, "Backing up Webroot \t\t", end='')
            AKRFileUtils.copyfiles(self, akr_site_webroot + '/htdocs', backup_path + '/htdocs')
            Log.info(self, "[" + Log.ENDC + "Done" + Log.OKBLUE + "]")
        else:
            Log.info(self, "Backing up Webroot \t\t", end='')
            AKRFileUtils.mvfile(self, akr_site_webroot + '/htdocs', backup_path)
            Log.info(self, "[" + Log.ENDC + "Done" + Log.OKBLUE + "]")

    configfiles = glob.glob(akr_site_webroot + '/*-config.php')
    if not configfiles:
        #search for np-konfig.php inside htdocs/
        Log.debug(self, "Config files not found in {0}/ "
                          .format(akr_site_webroot))
        if data['currsitetype'] in ['mysql']:
            pass
        else:
            Log.debug(self, "Searching np-konfig.php in {0}/htdocs/ "
                                   .format(akr_site_webroot))
            configfiles = glob.glob(akr_site_webroot + '/htdocs/np-konfig.php')

    # if configfiles and AKRFileUtils.isexist(self, configfiles[0]):
    #     akr_db_name = (AKRFileUtils.grep(self, configfiles[0],
    #                   'DB_NAME').split(',')[1]
    #                   .split(')')[0].strip().replace('\'', ''))
    if data['akr_db_name']:
        Log.info(self, 'Backing up database \t\t', end='')
        try:
            if not AKRShellExec.cmd_exec(self, "mysqldump {0} > {1}/{0}.sql"
                                        .format(data['akr_db_name'],
                                                backup_path)):
                Log.info(self,
                         "[" + Log.ENDC + Log.FAIL + "Fail" + Log.OKBLUE + "]")
                raise SiteError("mysqldump failed to backup database")
        except CommandExecutionError as e:
            Log.info(self, "[" + Log.ENDC + "Fail" + Log.OKBLUE + "]")
            raise SiteError("mysqldump failed to backup database")
        Log.info(self, "[" + Log.ENDC + "Done" + Log.OKBLUE + "]")
        # move np-konfig.php/akr-config.php to backup
        if data['currsitetype'] in ['mysql', 'proxy']:
            if (data['pagespeed'] is True or data['old_pagespeed_status'] is True) and not data['ik']:
                AKRFileUtils.copyfile(self, configfiles[0], backup_path)
            else:
                AKRFileUtils.mvfile(self, configfiles[0], backup_path)
        else:
            AKRFileUtils.copyfile(self, configfiles[0], backup_path)


def site_package_check(self, stype):
    apt_packages = []
    packages = []
    stack = AKRStackController()
    stack.app = self.app
    if stype in ['html', 'proxy', 'php', 'mysql', 'ik', 'iksubdir',
                 'iksubdomain']:
        Log.debug(self, "Setting apt_packages variable for Nginx")

        # Check if server has nginx-custom package
        if not (AKRAptGet.is_installed(self, 'nginx-custom') or  AKRAptGet.is_installed(self, 'nginx-mainline')):
            # check if Server has nginx-plus installed
            if AKRAptGet.is_installed(self, 'nginx-plus'):
                # do something
                # do post nginx installation configuration
                Log.info(self, "NGINX PLUS Detected ...")
                apt = ["nginx-plus"] + AKRVariables.akr_nginx
                #apt_packages = apt_packages + AKRVariables.akr_nginx
                stack.post_pref(apt, packages)

            else:
                apt_packages = apt_packages + AKRVariables.akr_nginx
        else:
            # Fix for Nginx white screen death
            if not AKRFileUtils.grep(self, '/etc/nginx/fastcgi_params',
                                    'SCRIPT_FILENAME'):
                with open('/etc/nginx/fastcgi_params', encoding='utf-8',
                          mode='a') as akr_nginx:
                    akr_nginx.write('fastcgi_param \tSCRIPT_FILENAME '
                                   '\t$request_filename;\n')

    if stype in ['php', 'mysql', 'ik', 'iksubdir', 'iksubdomain']:
        Log.debug(self, "Setting apt_packages variable for PHP")
        if not AKRAptGet.is_installed(self, 'php5-fpm'):
            apt_packages = apt_packages + AKRVariables.akr_php

    if stype in ['mysql', 'ik', 'iksubdir', 'iksubdomain']:
        Log.debug(self, "Setting apt_packages variable for MySQL")
        if not AKRShellExec.cmd_exec(self, "mysqladmin ping"):
            apt_packages = apt_packages + AKRVariables.akr_mysql
            packages = packages + [["https://raw.githubusercontent.com/"
                                    "major/MySQLTuner-perl/master/"
                                    "mysqltuner.pl", "/usr/bin/mysqltuner",
                                    "MySQLTuner"]]

    if stype in ['php', 'mysql', 'ik', 'iksubdir', 'iksubdomain']:
        Log.debug(self, "Setting apt_packages variable for Postfix")
        if not AKRAptGet.is_installed(self, 'postfix'):
            apt_packages = apt_packages + AKRVariables.akr_postfix

    if stype in ['ik', 'iksubdir', 'iksubdomain']:
        Log.debug(self, "Setting packages variable for IK-CLI")
        if not AKRShellExec.cmd_exec(self, "which ik"):
            packages = packages + [["https://github.com/ik-cli/ik-cli/"
                                    "releases/download/v{0}/"
                                    "ik-cli-{0}.phar"
                                    .format(AKRVariables.akr_ik_cli),
                                    "/usr/bin/ik", "IK-CLI"]]
    if self.app.pargs.ikredis:
        Log.debug(self, "Setting apt_packages variable for redis")
        if not AKRAptGet.is_installed(self, 'redis-server'):
            apt_packages = apt_packages + AKRVariables.akr_redis

        if os.path.isfile("/etc/nginx/nginx.conf") and (not
           os.path.isfile("/etc/nginx/common/redis.conf")):

            data = dict()
            Log.debug(self, 'Writting the nginx configuration to '
                      'file /etc/nginx/common/redis.conf')
            akr_nginx = open('/etc/nginx/common/redis.conf',
                            encoding='utf-8', mode='w')
            self.app.render((data), 'redis.mustache',
                            out=akr_nginx)
            akr_nginx.close()

        if os.path.isfile("/etc/nginx/nginx.conf") and (not
           os.path.isfile("/etc/nginx/common/redis-hhvm.conf")):

            data = dict()
            Log.debug(self, 'Writting the nginx configuration to '
                      'file /etc/nginx/common/redis-hhvm.conf')
            akr_nginx = open('/etc/nginx/common/redis-hhvm.conf',
                            encoding='utf-8', mode='w')
            self.app.render((data), 'redis-hhvm.mustache',
                            out=akr_nginx)
            akr_nginx.close()

        if os.path.isfile("/etc/nginx/conf.d/upstream.conf"):
            if not AKRFileUtils.grep(self, "/etc/nginx/conf.d/"
                                    "upstream.conf",
                                    "redis"):
                with open("/etc/nginx/conf.d/upstream.conf",
                          "a") as redis_file:
                    redis_file.write("upstream redis {\n"
                                     "    server 127.0.0.1:6379;\n"
                                     "    keepalive 10;\n}")

        if os.path.isfile("/etc/nginx/nginx.conf") and (not
           os.path.isfile("/etc/nginx/conf.d/redis.conf")):
            with open("/etc/nginx/conf.d/redis.conf", "a") as redis_file:
                redis_file.write("# Log format Settings\n"
                                 "log_format rt_cache_redis '$remote_addr $upstream_response_time $srcache_fetch_status [$time_local] '\n"
                                 "'$http_host \"$request\" $status $body_bytes_sent '\n"
                                 "'\"$http_referer\" \"$http_user_agent\"';\n")

    if self.app.pargs.hhvm:
        if platform.architecture()[0] is '32bit':
            Log.error(self, "HHVM is not supported by 32bit system")
        Log.debug(self, "Setting apt_packages variable for HHVM")
        if not AKRAptGet.is_installed(self, 'hhvm'):
            apt_packages = apt_packages + AKRVariables.akr_hhvm

        if os.path.isdir("/etc/nginx/common") and (not
           os.path.isfile("/etc/nginx/common/php-hhvm.conf")):
            data = dict()
            Log.debug(self, 'Writting the nginx configuration to '
                      'file /etc/nginx/common/php-hhvm.conf')
            akr_nginx = open('/etc/nginx/common/php-hhvm.conf',
                            encoding='utf-8', mode='w')
            self.app.render((data), 'php-hhvm.mustache',
                            out=akr_nginx)
            akr_nginx.close()

            Log.debug(self, 'Writting the nginx configuration to '
                      'file /etc/nginx/common/w3tc-hhvm.conf')
            akr_nginx = open('/etc/nginx/common/w3tc-hhvm.conf',
                            encoding='utf-8', mode='w')
            self.app.render((data), 'w3tc-hhvm.mustache', out=akr_nginx)
            akr_nginx.close()

            Log.debug(self, 'Writting the nginx configuration to '
                      'file /etc/nginx/common/ikfc-hhvm.conf')
            akr_nginx = open('/etc/nginx/common/ikfc-hhvm.conf',
                            encoding='utf-8', mode='w')
            self.app.render((data), 'ikfc-hhvm.mustache',
                            out=akr_nginx)
            akr_nginx.close()

            Log.debug(self, 'Writting the nginx configuration to '
                      'file /etc/nginx/common/iksc-hhvm.conf')
            akr_nginx = open('/etc/nginx/common/iksc-hhvm.conf',
                            encoding='utf-8', mode='w')
            self.app.render((data), 'iksc-hhvm.mustache',
                            out=akr_nginx)
            akr_nginx.close()

        if os.path.isfile("/etc/nginx/conf.d/upstream.conf"):
            if not AKRFileUtils.grep(self, "/etc/nginx/conf.d/upstream.conf",
                                          "hhvm"):
                with open("/etc/nginx/conf.d/upstream.conf", "a") as hhvm_file:
                    hhvm_file.write("upstream hhvm {\nserver 127.0.0.1:8000;\n"
                                    "server 127.0.0.1:9000 backup;\n}\n")

    # Check if Nginx is allready installed and Pagespeed config there or not
    # If not then copy pagespeed config
    if self.app.pargs.pagespeed:
        if (os.path.isfile('/etc/nginx/nginx.conf') and
           (not os.path.isfile('/etc/nginx/conf.d/pagespeed.conf'))):
            # Pagespeed configuration
            data = dict()
            Log.debug(self, 'Writting the Pagespeed Global '
                      'configuration to file /etc/nginx/conf.d/'
                      'pagespeed.conf')
            akr_nginx = open('/etc/nginx/conf.d/pagespeed.conf',
                            encoding='utf-8', mode='w')
            self.app.render((data), 'pagespeed-global.mustache',
                            out=akr_nginx)
            akr_nginx.close()

    return(stack.install(apt_packages=apt_packages, packages=packages,
                         disp_msg=False))


def updatewpuserpassword(self, akr_domain, akr_site_webroot):

    akr_ik_user = ''
    akr_ik_pass = ''
    AKRFileUtils.chdir(self, '{0}/htdocs/'.format(akr_site_webroot))

    # Check if akr_domain is nanopublishr install
    try:
        is_ik = AKRShellExec.cmd_exec(self, "ik --allow-root core"
                                     " version")
    except CommandExecutionError as e:
        raise SiteError("is NanoPublisher site? check command failed ")

    # Exit if akr_domain is not nanopublishr install
    if not is_ik:
        Log.error(self, "{0} does not seem to be a NanoPublisher site"
                  .format(akr_domain))

    try:
        akr_ik_user = input("Provide NanoPublisher user name [admin]: ")
    except Exception as e:
        Log.debug(self, "{0}".format(e))
        Log.error(self, "\nCould not update password")

    if akr_ik_user == "?":
        Log.info(self, "Fetching NanoPublisher user list")
        try:
            AKRShellExec.cmd_exec(self, "ik --allow-root user list "
                                 "--fields=user_login | grep -v user_login")
        except CommandExecutionError as e:
            raise SiteError("fetch ik userlist command failed")

    if not akr_ik_user:
        akr_ik_user = 'admin'

    try:
        is_user_exist = AKRShellExec.cmd_exec(self, "ik --allow-root user list "
                                             "--fields=user_login | grep {0}$ "
                                             .format(akr_ik_user))
    except CommandExecutionError as e:
        raise SiteError("if ik user exists check command failed")

    if is_user_exist:
        try:
            akr_ik_pass = getpass.getpass(prompt="Provide password for "
                                         "{0} user: "
                                         .format(akr_ik_user))

            while not akr_ik_pass:
                akr_ik_pass = getpass.getpass(prompt="Provide password for "
                                             "{0} user: "
                                             .format(akr_ik_user))
        except Exception as e:
            Log.debug(self, "{0}".format(e))
            raise SiteError("failed to read password input ")

        try:
            AKRShellExec.cmd_exec(self, "ik --allow-root user update {0}"
                                 "  --user_pass={1}"
                                 .format(akr_ik_user, akr_ik_pass))
        except CommandExecutionError as e:
            raise SiteError("ik user password update command failed")
        Log.info(self, "Password updated successfully")

    else:
        Log.error(self, "Invalid NanoPublisher user {0} for {1}."
                  .format(akr_ik_user, akr_domain))


def display_cache_settings(self, data):
    if data['iksc']:
        if data['multisite']:
            Log.info(self, "Configure IKSC:"
                     "\t\thttp://{0}/ik-gtkeepr/network/settings.php?"
                     "page=iksupercache"
                     .format(data['site_name']))
        else:
            Log.info(self, "Configure IKSC:"
                     "\t\thttp://{0}/ik-gtkeepr/options-general.php?"
                     "page=iksupercache"
                     .format(data['site_name']))

    if data['ikredis']:
        if data['multisite']:
            Log.info(self, "Configure redis-cache:"
                     "\thttp://{0}/ik-gtkeepr/network/settings.php?"
                     "page=redis-cache".format(data['site_name']))
        else:
            Log.info(self, "Configure redis-cache:"
                     "\thttp://{0}/ik-gtkeepr/options-general.php?"
                     "page=redis-cache".format(data['site_name']))
        Log.info(self, "Object Cache:\t\tEnable")

    if data['ikfc'] or data['w3tc']:
        if data['multisite']:
            Log.info(self, "Configure W3TC:"
                     "\t\thttp://{0}/ik-gtkeepr/network/admin.php?"
                     "page=w3tc_general".format(data['site_name']))
        else:
            Log.info(self, "Configure W3TC:"
                     "\t\thttp://{0}/ik-gtkeepr/admin.php?"
                     "page=w3tc_general".format(data['site_name']))

        if data['ikfc']:
            Log.info(self, "Page Cache:\t\tDisable")
        elif data['w3tc']:
            Log.info(self, "Page Cache:\t\tDisk Enhanced")
        Log.info(self, "Database Cache:\t\tMemcached")
        Log.info(self, "Object Cache:\t\tMemcached")
        Log.info(self, "Browser Cache:\t\tDisable")


def logwatch(self, logfiles):
    import zlib
    import base64
    import time
    from akr.core import logwatch

    def callback(filename, lines):
        for line in lines:
            if line.find(':::') == -1:
                print(line)
            else:
                data = line.split(':::')
                try:
                    print(data[0], data[1],
                          zlib.decompress(base64.decodestring(data[2])))
                except Exception as e:
                    Log.info(time.time(),
                             'caught exception rendering a new log line in %s'
                             % filename)

    l = logwatch.LogWatcher(logfiles, callback)
    l.loop()


def detSitePar(opts):
    """
        Takes dictionary of parsed arguments
        1.returns sitetype and cachetype
        2. raises RuntimeError when wrong combination is used like
            "--ik --iksubdir" or "--html --ik"
    """
    sitetype, cachetype = '', ''
    typelist = list()
    cachelist = list()
    for key, val in opts.items():
        if val and key in ['html', 'php', 'mysql', 'ik',
                           'iksubdir', 'iksubdomain']:
            typelist.append(key)
        elif val and key in ['ikfc', 'iksc', 'w3tc', 'ikredis']:
            cachelist.append(key)

    if len(typelist) > 1 or len(cachelist) > 1:
        if len(cachelist) > 1:
            raise RuntimeError("Could not determine cache type.Multiple cache parameter entered")
        elif False not in [x in ('php','mysql','html') for x in typelist]:
            sitetype = 'mysql'
            if not cachelist:
                cachetype = 'basic'
            else:
                cachetype = cachelist[0]
        elif False not in [x in ('php','mysql') for x in typelist]:
            sitetype = 'mysql'
            if not cachelist:
                cachetype = 'basic'
            else:
                cachetype = cachelist[0]
        elif False not in [x in ('html','mysql') for x in typelist]:
            sitetype = 'mysql'
            if not cachelist:
                cachetype = 'basic'
            else:
                cachetype = cachelist[0]
        elif False not in [x in ('php','html') for x in typelist]:
            sitetype = 'php'
            if not cachelist:
                cachetype = 'basic'
            else:
                cachetype = cachelist[0]
        elif False not in [x in ('ik','iksubdir') for x in typelist]:
            sitetype = 'iksubdir'
            if not cachelist:
                cachetype = 'basic'
            else:
                cachetype = cachelist[0]
        elif False not in [x in ('ik','iksubdomain') for x in typelist]:
            sitetype = 'iksubdomain'
            if not cachelist:
                cachetype = 'basic'
            else:
                cachetype = cachelist[0]
        else:
            raise RuntimeError("could not determine site and cache type")

    else:
        if not typelist and not cachelist:
            sitetype = None
            cachetype = None
        elif (not typelist) and cachelist:
            sitetype = 'ik'
            cachetype = cachelist[0]
        elif typelist and (not cachelist):
            sitetype = typelist[0]
            cachetype = 'basic'
        else:
            sitetype = typelist[0]
            cachetype = cachelist[0]
    return (sitetype, cachetype)


def generate_random():
    akr_random10 = (''.join(random.sample(string.ascii_uppercase +
                   string.ascii_lowercase + string.digits, 10)))
    return akr_random10


def deleteDB(self, dbname, dbuser, dbhost, exit=True):
    try:
        # Check if Database exists
        try:
            if AKRMysql.check_db_exists(self, dbname):
                # Drop database if exists
                Log.debug(self, "dropping database `{0}`".format(dbname))
                AKRMysql.execute(self,
                                "drop database `{0}`".format(dbname),
                                errormsg='Unable to drop database {0}'
                                .format(dbname))
        except StatementExcecutionError as e:
            Log.debug(self, "drop database failed")
            Log.info(self, "Database {0} not dropped".format(dbname))

        except MySQLConnectionError as e:
            Log.debug(self, "Mysql Connection problem occured")

        if dbuser != 'root':
            Log.debug(self, "dropping user `{0}`".format(dbuser))
            try:
                AKRMysql.execute(self,
                                "drop user `{0}`@`{1}`"
                                .format(dbuser, dbhost))
            except StatementExcecutionError as e:
                Log.debug(self, "drop database user failed")
                Log.info(self, "Database {0} not dropped".format(dbuser))
            try:
                AKRMysql.execute(self, "flush privileges")
            except StatementExcecutionError as e:
                Log.debug(self, "drop database failed")
                Log.info(self, "Database {0} not dropped".format(dbname))
    except Exception as e:
        Log.error(self, "Error occured while deleting database", exit)


def deleteWebRoot(self, webroot):
    # do some preprocessing before proceeding
    webroot = webroot.strip()
    if (webroot == "/var/www/" or webroot == "/var/www"
       or webroot == "/var/www/.." or webroot == "/var/www/."):
        Log.debug(self, "Tried to remove {0}, but didn't remove it"
                  .format(webroot))
        return False

    if os.path.isdir(webroot):
        Log.debug(self, "Removing {0}".format(webroot))
        AKRFileUtils.rm(self, webroot)
        return True
    else:
        Log.debug(self, "{0} does not exist".format(webroot))
        return False


def removeNginxConf(self, domain):
    if os.path.isfile('/etc/nginx/sites-available/{0}'
                      .format(domain)):
            Log.debug(self, "Removing Nginx configuration")
            AKRFileUtils.rm(self, '/etc/nginx/sites-enabled/{0}'
                           .format(domain))
            AKRFileUtils.rm(self, '/etc/nginx/sites-available/{0}'
                           .format(domain))
            AKRService.reload_service(self, 'nginx')
            AKRGit.add(self, ["/etc/nginx"],
                      msg="Deleted {0} "
                      .format(domain))


def doCleanupAction(self, domain='', webroot='', dbname='', dbuser='',
                    dbhost=''):
    """
       Removes the nginx configuration and database for the domain provided.
       doCleanupAction(self, domain='sitename', webroot='',
                       dbname='', dbuser='', dbhost='')
    """
    if domain:
        if os.path.isfile('/etc/nginx/sites-available/{0}'
                          .format(domain)):
            removeNginxConf(self, domain)
    if webroot:
        deleteWebRoot(self, webroot)

    if dbname:
        if not dbuser:
            raise SiteError("dbuser not provided")
            if not dbhost:
                raise SiteError("dbhost not provided")
        deleteDB(self, dbname, dbuser, dbhost)


def operateOnPagespeed(self, data):

    akr_domain_name = data['site_name']
    akr_site_webroot = data['webroot']

    if data['pagespeed'] is True:
        if not os.path.isfile("{0}/conf/nginx/pagespeed.conf.disabled"
                              .format(akr_site_webroot)):
            Log.debug(self, 'Writting the Pagespeed common '
                      'configuration to file {0}/conf/nginx/pagespeed.conf'
                      'pagespeed.conf'.format(akr_site_webroot))
            akr_nginx = open('{0}/conf/nginx/pagespeed.conf'
                            .format(akr_site_webroot), encoding='utf-8',
                            mode='w')
            self.app.render((data), 'pagespeed-common.mustache',
                            out=akr_nginx)
            akr_nginx.close()
        else:
            AKRFileUtils.mvfile(self, "{0}/conf/nginx/pagespeed.conf.disabled"
                               .format(akr_site_webroot),
                               '{0}/conf/nginx/pagespeed.conf'
                               .format(akr_site_webroot))

    elif data['pagespeed'] is False:
        if os.path.isfile("{0}/conf/nginx/pagespeed.conf"
                          .format(akr_site_webroot)):
            AKRFileUtils.mvfile(self, "{0}/conf/nginx/pagespeed.conf"
                               .format(akr_site_webroot),
                               '{0}/conf/nginx/pagespeed.conf.disabled'
                               .format(akr_site_webroot))

    # Add nginx conf folder into GIT
    AKRGit.add(self, ["{0}/conf/nginx".format(akr_site_webroot)],
              msg="Adding Pagespeed config of site: {0}"
              .format(akr_domain_name))

def cloneLetsEncrypt(self):
    letsencrypt_repo = "https://github.com/letsencrypt/letsencrypt"
    if not os.path.isdir("/opt"):
        AKRFileUtils.mkdir(self,"/opt")
    try:
        Log.info(self, "Downloading {0:20}".format("LetsEncrypt"), end=' ')
        AKRFileUtils.chdir(self, '/opt/')
        AKRShellExec.cmd_exec(self, "git clone {0}".format(letsencrypt_repo))
        Log.info(self, "{0}".format("[" + Log.ENDC + "Done"
                                            + Log.OKBLUE + "]"))
        return True
    except Exception as e:
        Log.debug(self, "[{err}]".format(err=str(e.reason)))
        Log.error(self, "Unable to download file, LetsEncrypt")
        return False

def setupLetsEncrypt(self, akr_domain_name):
    akr_ik_email = AKRVariables.akr_email
    while not akr_ik_email:
        try:
            akr_ik_email = input('Enter NanoPublisher email: ')
        except EOFError as e:
            Log.debug(self, "{0}".format(e))
            raise SiteError("input NanoPublisher username failed")

    if not os.path.isdir("/opt/letsencrypt"):
        cloneLetsEncrypt(self)
    AKRFileUtils.chdir(self, '/opt/letsencrypt')
    AKRShellExec.cmd_exec(self, "git pull")

    if os.path.isfile("/etc/letsencrypt/renewal/{0}.conf".format(akr_domain_name)):
        Log.debug(self, "LetsEncrypt SSL Certificate found for the domain {0}"
                 .format(akr_domain_name))
        ssl= archivedCertificateHandle(self,akr_domain_name,akr_ik_email)
    else:
        Log.warn(self,"Please Wait while we fetch SSL Certificate for your site.\nIt may take time depending upon network.")
        ssl = AKRShellExec.cmd_exec(self, "./letsencrypt-auto certonly --webroot -w /var/www/{0}/htdocs/ -d {0} -d www.{0} "
                                .format(akr_domain_name)
                                + "--email {0} --text --agree-tos".format(akr_ik_email))
    if ssl:
        Log.info(self, "Let's Encrypt successfully setup for your site")
        Log.info(self, "Your certificate and chain have been saved at "
                            "/etc/letsencrypt/live/{0}/fullchain.pem".format(akr_domain_name))
        Log.info(self, "Configuring Nginx SSL configuration")

        try:
            Log.info(self, "Adding /var/www/{0}/conf/nginx/ssl.conf".format(akr_domain_name))

            sslconf = open("/var/www/{0}/conf/nginx/ssl.conf"
                                      .format(akr_domain_name),
                                      encoding='utf-8', mode='w')
            sslconf.write("listen 443 ssl {http2};\n".format(http2=("http2" if
                                                           AKRAptGet.is_installed(self,'nginx-mainline') else "spdy")) +
                                     "ssl on;\n"
                                     "ssl_certificate     /etc/letsencrypt/live/{0}/fullchain.pem;\n"
                                     "ssl_certificate_key     /etc/letsencrypt/live/{0}/privkey.pem;\n"
                                     .format(akr_domain_name))
            sslconf.close()
            # updateSiteInfo(self, akr_domain_name, ssl=True)

            AKRGit.add(self, ["/etc/letsencrypt"],
              msg="Adding letsencrypt folder")

        except IOError as e:
            Log.debug(self, str(e))
            Log.debug(self, "Error occured while generating "
                              "ssl.conf")
    else:
        Log.error(self, "Unable to setup, Let\'s Encrypt", False)
        Log.error(self, "Please make sure that your site is pointed to \n"
                        "same server on which you are running Let\'s Encrypt Client "
                        "\n to allow it to verify the site automatically.")

def renewLetsEncrypt(self, akr_domain_name):

    akr_ik_email = AKRVariables.akr_email
    while not akr_ik_email:
        try:
            akr_ik_email = input('Enter email address: ')
        except EOFError as e:
            Log.debug(self, "{0}".format(e))
            raise SiteError("Input NanoPublisher email failed")

    if not os.path.isdir("/opt/letsencrypt"):
        cloneLetsEncrypt(self)
    AKRFileUtils.chdir(self, '/opt/letsencrypt')
    AKRShellExec.cmd_exec(self, "git pull")

    Log.info(self, "Renewing SSl cert for https://{0}".format(akr_domain_name))

    ssl = AKRShellExec.cmd_exec(self, "./letsencrypt-auto --renew certonly --webroot -w /var/www/{0}/htdocs/ -d {0} -d www.{0} "
                                .format(akr_domain_name)
                                + "--email {0} --text --agree-tos".format(akr_ik_email))
    mail_list = ''
    if not ssl:
        Log.error(self,"ERROR : Cannot RENEW SSL cert !",False)
        if (SSL.getExpirationDays(self,akr_domain_name)>0):
                    Log.error(self, "Your current cert will expire within " + str(SSL.getExpirationDays(self,akr_domain_name)) + " days.",False)
        else:
                    Log.error(self, "Your current cert already EXPIRED !",False)

        AKRSendMail("akur8engine@{0}".format(akr_domain_name), akr_ik_email, "[FAIL] SSL cert renewal {0}".format(akr_domain_name),
                       "Hey Hi,\n\nSSL Certificate renewal for https://{0} was unsuccessful.".format(akr_domain_name) +
                       "\nPlease check akur8engine log for reason. Your SSL Expiry date : " +
                            str(SSL.getExpirationDate(self,akr_domain_name)) +
                       "\n\nFor support visit https://akur8engine.io/support/ .\n\nYour's faithfully,\nAkur8Engine",files=mail_list,
                        port=25, isTls=False)
        Log.error(self, "Check logs for reason "
                      "`tail /var/log/akr/akr.log` & Try Again!!!")

    AKRGit.add(self, ["/etc/letsencrypt"],
              msg="Adding letsencrypt folder")
    AKRSendMail("akur8engine@{0}".format(akr_domain_name), akr_ik_email, "[SUCCESS] SSL cert renewal {0}".format(akr_domain_name),
                       "Hey Hi,\n\nYour SSL Certificate has been renewed for https://{0} .".format(akr_domain_name) +
                       "\nYour SSL will Expire on : " +
                            str(SSL.getExpirationDate(self,akr_domain_name)) +
                       "\n\nYour's faithfully,\nAkur8Engine",files=mail_list,
                        port=25, isTls=False)

#redirect= False to disable https redirection
def httpsRedirect(self,akr_domain_name,redirect=True):
    if redirect:
        if os.path.isfile("/etc/nginx/conf.d/force-ssl-{0}.conf.disabled".format(akr_domain_name)):
                AKRFileUtils.mvfile(self, "/etc/nginx/conf.d/force-ssl-{0}.conf.disabled".format(akr_domain_name),
                                  "/etc/nginx/conf.d/force-ssl-{0}.conf".format(akr_domain_name))
        else:
            try:
                Log.info(self, "Adding /etc/nginx/conf.d/force-ssl-{0}.conf".format(akr_domain_name))

                sslconf = open("/etc/nginx/conf.d/force-ssl-{0}.conf"
                                      .format(akr_domain_name),
                                      encoding='utf-8', mode='w')
                sslconf.write("server {\n"
                                     "\tlisten 80;\n" +
                                     "\tserver_name www.{0} {0};\n".format(akr_domain_name) +
                                     "\treturn 301 https://{0}".format(akr_domain_name)+"$request_uri;\n}" )
                sslconf.close()
                # Nginx Configation into GIT
            except IOError as e:
                Log.debug(self, str(e))
                Log.debug(self, "Error occured while generating "
                              "/etc/nginx/conf.d/force-ssl-{0}.conf".format(akr_domain_name))

        Log.info(self, "Added HTTPS Force Redirection for Site "
                         " http://{0}".format(akr_domain_name))
        AKRGit.add(self,
                  ["/etc/nginx"], msg="Adding /etc/nginx/conf.d/force-ssl-{0}.conf".format(akr_domain_name))
    else:
        if os.path.isfile("/etc/nginx/conf.d/force-ssl-{0}.conf".format(akr_domain_name)):
             AKRFileUtils.mvfile(self, "/etc/nginx/conf.d/force-ssl-{0}.conf".format(akr_domain_name),
                                  "/etc/nginx/conf.d/force-ssl-{0}.conf.disabled".format(akr_domain_name))
             Log.info(self, "Disabled HTTPS Force Redirection for Site "
                         " http://{0}".format(akr_domain_name))

def archivedCertificateHandle(self,domain,akr_ik_email):
    Log.warn(self,"You already have an existing certificate for the domain requested.\n"
                        "(ref: /etc/letsencrypt/renewal/{0}.conf)".format(domain) +
                        "\nPlease select an option from below?"
                    "\n\t1: Reinstall existing certificate"
                    "\n\t2: Keep the existing certificate for now"
                    "\n\t3: Renew & replace the certificate (limit ~5 per 7 days)"
                        "")
    check_prompt = input("\nType the appropriate number [1-3] or any other key to cancel: ")
    if not os.path.isfile("/etc/letsencrypt/live/{0}/cert.pem".format(domain)):
            Log.error(self,"/etc/letsencrypt/live/{0}/cert.pem file is missing.".format(domain))
    if check_prompt == "1":
        Log.info(self,"Please Wait while we reinstall SSL Certificate for your site.\nIt may take time depending upon network.")
        ssl = AKRShellExec.cmd_exec(self, "./letsencrypt-auto certonly --reinstall --webroot -w /var/www/{0}/htdocs/ -d {0} -d www.{0} "
                                .format(domain)
                                + "--email {0} --text --agree-tos".format(akr_ik_email))
    elif check_prompt == "2" :
        Log.info(self,"Using Existing Certificate files")
        if not (os.path.isfile("/etc/letsencrypt/live/{0}/fullchain.pem".format(domain)) or
                    os.path.isfile("/etc/letsencrypt/live/{0}/privkey.pem".format(domain))):
            Log.error(self,"Certificate files not found. Skipping.\n"
                           "Please check if following file exist\n\t/etc/letsencrypt/live/{0}/fullchain.pem\n\t"
                           "/etc/letsencrypt/live/{0}/privkey.pem".format(domain))
        ssl = True

    elif check_prompt == "3":
        Log.info(self,"Please Wait while we renew SSL Certificate for your site.\nIt may take time depending upon network.")
        ssl = AKRShellExec.cmd_exec(self, "./letsencrypt-auto --renew certonly --webroot -w /var/www/{0}/htdocs/ -d {0} -d www.{0} "
                                .format(domain)
                                + "--email {0} --text --agree-tos".format(akr_ik_email))
    else:
        Log.error(self,"Operation cancelled by user.")

    if os.path.isfile("{0}/conf/nginx/ssl.conf"
                              .format(domain)):
        Log.info(self, "Existing ssl.conf . Backing it up ..")
        AKRFileUtils.mvfile(self, "/var/www/{0}/conf/nginx/ssl.conf"
                             .format(domain),
                             '/var/www/{0}/conf/nginx/ssl.conf.bak'
                             .format(domain))

    return ssl




