from cement.core.controller import CementBaseController, expose
from cement.core import handler, hook
from akr.core.fileutils import AKRFileUtils
from akr.cli.npextionsions.sitedb import *
from akr.core.mysql import *
from akr.core.logging import Log


def akr_sync_hook(app):
    # do something with the ``app`` object here.
    pass


class AKRSyncController(CementBaseController):
    class Meta:
        label = 'sync'
        stacked_on = 'base'
        stacked_type = 'nested'
        description = 'synchronize Akur8Engine database'

    @expose(hide=True)
    def default(self):
        self.sync()

    @expose(hide=True)
    def sync(self):
        """
        1. reads database information from ik/akr-config.php
        2. updates records into akr database accordingly.
        """
        Log.info(self, "Synchronizing akr database, please wait...")
        sites = getAllsites(self)
        if not sites:
            pass
        for site in sites:
            if site.site_type in ['mysql', 'ik', 'iksubdir', 'iksubdomain']:
                akr_site_webroot = site.site_path
                # Read config files
                configfiles = glob.glob(akr_site_webroot + '/*-config.php')

                #search for np-konfig.php inside htdocs/
                if not configfiles:
                    Log.debug(self, "Config files not found in {0}/ "
                                      .format(akr_site_webroot))
                    if site.site_type != 'mysql':
                        Log.debug(self, "Searching np-konfig.php in {0}/htdocs/ "
                                      .format(akr_site_webroot))
                        configfiles = glob.glob(akr_site_webroot + '/htdocs/np-konfig.php')

                if configfiles:
                    if AKRFileUtils.isexist(self, configfiles[0]):
                        akr_db_name = (AKRFileUtils.grep(self, configfiles[0],
                                      'DB_NAME').split(',')[1]
                                      .split(')')[0].strip().replace('\'', ''))
                        akr_db_user = (AKRFileUtils.grep(self, configfiles[0],
                                      'DB_USER').split(',')[1]
                                      .split(')')[0].strip().replace('\'', ''))
                        akr_db_pass = (AKRFileUtils.grep(self, configfiles[0],
                                      'DB_PASSWORD').split(',')[1]
                                      .split(')')[0].strip().replace('\'', ''))
                        akr_db_host = (AKRFileUtils.grep(self, configfiles[0],
                                      'DB_HOST').split(',')[1]
                                      .split(')')[0].strip().replace('\'', ''))

                        # Check if database really exist
                        try:
                            if not AKRMysql.check_db_exists(self, akr_db_name):
                                # Mark it as deleted if not exist
                                akr_db_name = 'deleted'
                                akr_db_user = 'deleted'
                                akr_db_pass = 'deleted'
                        except StatementExcecutionError as e:
                            Log.debug(self, str(e))
                        except Exception as e:
                            Log.debug(self, str(e))

                        if site.db_name != akr_db_name:
                            # update records if any mismatch found
                            Log.debug(self, "Updating akr db record for {0}"
                                      .format(site.sitename))
                            updateSiteInfo(self, site.sitename,
                                           db_name=akr_db_name,
                                           db_user=akr_db_user,
                                           db_password=akr_db_pass,
                                           db_host=akr_db_host)
                else:
                    Log.debug(self, "Config files not found for {0} "
                                      .format(site.sitename))


def load(app):
    # register the npextionsion class.. this only happens if the npextionsion is enabled
    handler.register(AKRSyncController)
    # register a hook (function) to run after arguments are parsed.
    hook.register('post_argument_parsing', akr_sync_hook)
