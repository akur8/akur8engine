from cement.core.controller import CementBaseController, expose
from cement.core import handler, hook
from akr.core.shellexec import AKRShellExec
from akr.core.logging import Log
from akr.core.variables import AKRVariables
import os


def akr_import_slow_log_hook(app):
    pass


class AKRImportslowlogController(CementBaseController):
    class Meta:
        label = 'import_slow_log'
        stacked_on = 'base'
        stacked_type = 'nested'
        description = 'Import MySQL slow log to Anemometer database'
        usage = "akr import-slow-log"

    @expose(hide=True)
    def default(self):
        Log.info(self, "This command is deprecated."
                 " You can use this command instead, " +
                 Log.ENDC + Log.BOLD + "\n`akr debug --import-slow-log`" +
                 Log.ENDC)


def load(app):
    # register the npextionsion class.. this only happens if the npextionsion is enabled
    handler.register(AKRImportslowlogController)

    # register a hook (function) to run after arguments are parsed.
    hook.register('post_argument_parsing', akr_import_slow_log_hook)
