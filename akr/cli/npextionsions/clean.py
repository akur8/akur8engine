"""Clean Npextension for Akur8Engine."""

from akr.core.shellexec import AKRShellExec
from akr.core.aptget import AKRAptGet
from akr.core.services import AKRService
from akr.core.logging import Log
from cement.core.controller import CementBaseController, expose
from cement.core import handler, hook
import os
import urllib.request


def akr_clean_hook(app):
    # do something with the ``app`` object here.
    pass


class AKRCleanController(CementBaseController):
    class Meta:
        label = 'clean'
        stacked_on = 'base'
        stacked_type = 'nested'
        description = ('Clean NGINX FastCGI cache, Opcacache, Memcache, Pagespeed Cache, Redis Cache')
        arguments = [
            (['--all'],
                dict(help='Clean all cache', action='store_true')),
            (['--fastcgi'],
                dict(help='Clean FastCGI cache', action='store_true')),
            (['--memcache'],
                dict(help='Clean MemCache', action='store_true')),
            (['--opcache'],
                dict(help='Clean OpCache', action='store_true')),
            (['--pagespeed'],
                dict(help='Clean Pagespeed Cache', action='store_true')),
            (['--redis'],
                dict(help='Clean Redis Cache', action='store_true')),
            ]
        usage = "akr clean [options]"

    @expose(hide=True)
    def default(self):
        if (not (self.app.pargs.all or self.app.pargs.fastcgi or
                 self.app.pargs.memcache or self.app.pargs.opcache or
                 self.app.pargs.pagespeed or self.app.pargs.redis)):
            self.clean_fastcgi()
        if self.app.pargs.all:
            self.clean_memcache()
            self.clean_fastcgi()
            self.clean_opcache()
            self.clean_redis()
            self.clean_pagespeed()
        if self.app.pargs.fastcgi:
            self.clean_fastcgi()
        if self.app.pargs.memcache:
            self.clean_memcache()
        if self.app.pargs.opcache:
            self.clean_opcache()
        if self.app.pargs.pagespeed:
            self.clean_pagespeed()
        if self.app.pargs.redis:
            self.clean_redis()

    @expose(hide=True)
    def clean_redis(self):
        """This function clears Redis cache"""
        if(AKRAptGet.is_installed(self, "redis-server")):
            Log.info(self, "Cleaning Redis cache")
            AKRShellExec.cmd_exec(self, "redis-cli flushall")
        else:
            Log.info(self, "Redis is not installed")

    @expose(hide=True)
    def clean_memcache(self):
        """This function Clears memcache """
        try:
            if(AKRAptGet.is_installed(self, "memcached")):
                AKRService.restart_service(self, "memcached")
                Log.info(self, "Cleaning MemCache")
            else:
                Log.info(self, "Memcache not installed")
        except Exception as e:
            Log.debug(self, "{0}".format(e))
            Log.error(self, "Unable to restart Memcached", False)

    @expose(hide=True)
    def clean_fastcgi(self):
        """This function clears Fastcgi cache"""
        if(os.path.isdir("/var/run/nginx-cache")):
            Log.info(self, "Cleaning NGINX FastCGI cache")
            AKRShellExec.cmd_exec(self, "rm -rf /var/run/nginx-cache/*")
        else:
            Log.error(self, "Unable to clean FastCGI cache", False)

    @expose(hide=True)
    def clean_opcache(self):
        """This function clears opcache"""
        try:
            Log.info(self, "Cleaning opcache")
            ik = urllib.request.urlopen(" https://127.0.0.1:22222/cache"
                                        "/opcache/opgui.php?page=reset").read()
        except Exception as e:
                Log.debug(self, "{0}".format(e))
                Log.debug(self, "Unable hit url, "
                          " https://127.0.0.1:22222/cache/opcache/opgui.php?page=reset,"
                          " please check you have admin tools installed")
                Log.debug(self, "please check you have admin tools installed,"
                         " or install them with `akr stack install --admin`")
                Log.error(self, "Unable to clean opcache", False)

    @expose(hide=True)
    def clean_pagespeed(self):
        """This function clears Pagespeed cache"""
        if(os.path.isdir("/var/ngx_pagespeed_cache")):
            Log.info(self, "Cleaning PageSpeed cache")
            AKRShellExec.cmd_exec(self, "rm -rf /var/ngx_pagespeed_cache/*")
        else:
            Log.debug(self, "/var/ngx_pagespeed_cache does not exist," 
                            " so cache not cleared")
            Log.error(self, "Unable to clean pagespeed cache", False)


def load(app):
    # register the npextionsion class.. this only happens if the npextionsion is enabled
    handler.register(AKRCleanController)
    # register a hook (function) to run after arguments are parsed.
    hook.register('post_argument_parsing', akr_clean_hook)
