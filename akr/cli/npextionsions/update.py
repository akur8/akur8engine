from cement.core.controller import CementBaseController, expose
from cement.core import handler, hook
from akr.core.download import AKRDownload
from akr.core.logging import Log
import time
import os


def akr_update_hook(app):
    # do something with the ``app`` object here.
    pass


class AKRUpdateController(CementBaseController):
    class Meta:
        label = 'akr_update'
        stacked_on = 'base'
        aliases = ['update']
        aliases_only = True
        stacked_type = 'nested'
        description = ('update Akur8Engine to latest version')
        usage = "akr update"

    @expose(hide=True)
    def default(self):
        filename = "akrupdate" + time.strftime("%Y%m%d-%H%M%S")
        AKRDownload.download(self, [["http://rt.cx/akrup",
                                    "/tmp/{0}".format(filename),
                                    "update script"]])
        try:
            Log.info(self, "updating Akur8Engine, please wait...")
            os.system("bash /tmp/{0}".format(filename))
        except OSError as e:
            Log.debug(self, str(e))
            Log.error(self, "Akur8Engine update failed !")
        except Exception as e:
            Log.debug(self, str(e))
            Log.error(self, "Akur8Engine update failed !")


def load(app):
    # register the npextionsion class.. this only happens if the npextionsion is enabled
    handler.register(AKRUpdateController)
    # register a hook (function) to run after arguments are parsed.
    hook.register('post_argument_parsing', akr_update_hook)
