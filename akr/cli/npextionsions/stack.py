"""Stack Npextension for Akur8Engine."""

from cement.core.controller import CementBaseController, expose
from cement.core import handler, hook
from akr.cli.npextionsions.site_functions import *
from akr.core.variables import AKRVariables
from akr.core.aptget import AKRAptGet
from akr.core.download import AKRDownload
from akr.core.shellexec import AKRShellExec, CommandExecutionError
from akr.core.fileutils import AKRFileUtils
from akr.core.apt_repo import AKRRepo
from akr.core.extract import AKRExtract
from akr.core.mysql import AKRMysql
from akr.core.addswap import AKRSwap
from akr.core.git import AKRGit
from akr.core.checkfqdn import check_fqdn
from pynginxconfig import NginxConfig
from akr.core.services import AKRService
from akr.core.variables import AKRVariables
import random
import string
import configparser
import time
import shutil
import os
import pwd
import grp
import codecs
import platform
from akr.cli.npextionsions.stack_services import AKRStackStatusController
from akr.cli.npextionsions.stack_migrate import AKRStackMigrateController
from akr.cli.npextionsions.stack_upgrade import AKRStackUpgradeController
from akr.core.logging import Log
from akr.cli.npextionsions.sitedb import *


def akr_stack_hook(app):
    # do something with the ``app`` object here.
    pass


class AKRStackController(CementBaseController):
    class Meta:
        label = 'stack'
        stacked_on = 'base'
        stacked_type = 'nested'
        description = 'Stack command manages stack operations'
        arguments = [
            (['--all'],
                dict(help='Install all stack', action='store_true')),
            (['--web'],
                dict(help='Install web stack', action='store_true')),
            (['--admin'],
                dict(help='Install admin tools stack', action='store_true')),
            (['--mail'],
                dict(help='Install mail server stack', action='store_true')),
            (['--mailscanner'],
                dict(help='Install mail scanner stack', action='store_true')),
            (['--nginx'],
                dict(help='Install Nginx stack', action='store_true')),
            (['--nginxmainline'],
                dict(help='Install Nginx mainline stack', action='store_true')),
            (['--php'],
                dict(help='Install PHP stack', action='store_true')),
            (['--mysql'],
                dict(help='Install MySQL stack', action='store_true')),
            (['--hhvm'],
                dict(help='Install HHVM stack', action='store_true')),
            (['--postfix'],
                dict(help='Install Postfix stack', action='store_true')),
            (['--ikcli'],
                dict(help='Install IKCLI stack', action='store_true')),
            (['--phpmyadmin'],
                dict(help='Install PHPMyAdmin stack', action='store_true')),
            (['--adminer'],
                dict(help='Install Adminer stack', action='store_true')),
            (['--utils'],
                dict(help='Install Utils stack', action='store_true')),
            (['--pagespeed'],
                dict(help='Install Pagespeed', action='store_true')),
            (['--redis'],
                dict(help='Install Redis', action='store_true')),
            (['--phpredisadmin'],
                dict(help='Install phpRedisAdmin', action='store_true')),
            ]
        usage = "akr stack (command) [options]"

    @expose(hide=True)
    def default(self):
        """default action of akr stack command"""
        self.app.args.print_help()

    @expose(hide=True)
    def pre_pref(self, apt_packages):
        """Pre settings to do before installation packages"""
        if set(AKRVariables.akr_postfix).issubset(set(apt_packages)):
            Log.debug(self, "Pre-seeding Postfix")
            try:
                AKRShellExec.cmd_exec(self, "echo \"postfix postfix"
                                     "/main_mailer_type string \'Internet Site"
                                     "\'\""
                                     " | debconf-set-selections")
                AKRShellExec.cmd_exec(self, "echo \"postfix postfix/mailname"
                                     " string $(hostname -f)\" | "
                                     "debconf-set-selections")
            except CommandExecutionError as e:
                Log.error(self, "Failed to intialize postfix package")

        if set(AKRVariables.akr_mysql).issubset(set(apt_packages)):
            Log.info(self, "Adding repository for MySQL, please wait...")
            mysql_pref = ("Package: *\nPin: origin sfo1.mirrors.digitalocean.com"
                          "\nPin-Priority: 1000\n")
            with open('/etc/apt/preferences.d/'
                      'MariaDB.pref', 'w') as mysql_pref_file:
                mysql_pref_file.write(mysql_pref)
        #    if AKRVariables.akr_platform_codename != 'jessie':
            AKRRepo.add(self, repo_url=AKRVariables.akr_mysql_repo)
            Log.debug(self, 'Adding key for {0}'
                        .format(AKRVariables.akr_mysql_repo))
            AKRRepo.add_key(self, '0xcbcb082a1bb943db',
                               keyserver="keyserver.ubuntu.com")
            chars = ''.join(random.sample(string.ascii_letters, 8))
            Log.debug(self, "Pre-seeding MySQL")
            Log.debug(self, "echo \"mariadb-server-10.1 "
                      "mysql-server/root_password "
                      "password \" | "
                      "debconf-set-selections")
            try:
                AKRShellExec.cmd_exec(self, "echo \"mariadb-server-10.1 "
                                     "mysql-server/root_password "
                                     "password {chars}\" | "
                                     "debconf-set-selections"
                                     .format(chars=chars),
                                     log=False)
            except CommandExecutionError as e:
                Log.error("Failed to initialize MySQL package")

            Log.debug(self, "echo \"mariadb-server-10.1 "
                      "mysql-server/root_password_again "
                      "password \" | "
                      "debconf-set-selections")
            try:
                AKRShellExec.cmd_exec(self, "echo \"mariadb-server-10.1 "
                                     "mysql-server/root_password_again "
                                     "password {chars}\" | "
                                     "debconf-set-selections"
                                     .format(chars=chars),
                                     log=False)
            except CommandExecutionError as e:
                Log.error("Failed to initialize MySQL package")

            mysql_config = """
            [client]
            user = root
            password = {chars}
            """.format(chars=chars)
            config = configparser.ConfigParser()
            config.read_string(mysql_config)
            Log.debug(self, 'Writting configuration into MySQL file')
            conf_path = "/etc/mysql/conf.d/my.cnf"
            os.makedirs(os.path.dirname(conf_path), exist_ok=True)
            with open(conf_path, encoding='utf-8',
                      mode='w') as configfile:
                config.write(configfile)
            Log.debug(self, 'Setting my.cnf permission')
            AKRFileUtils.chmod(self, "/etc/mysql/conf.d/my.cnf", 0o600)


        if set(AKRVariables.akr_nginx).issubset(set(apt_packages)):
            Log.info(self, "Adding repository for NGINX, please wait...")
            AKRRepo.add(self, repo_url=AKRVariables.akr_nginx_repo)
            Log.debug(self, 'Adding ppa of Nginx')
            AKRRepo.add_key(self, AKRVariables.akr_nginx_key)

        if set(["nginx-mainline"]).issubset(set(apt_packages)):
            Log.info(self, "Adding repository for NGINX MAINLINE, please wait...")
            AKRRepo.add(self, repo_url=AKRVariables.akr_nginx_dev_repo)
            Log.debug(self, 'Adding ppa of Nginx-mainline')
            AKRRepo.add_key(self, AKRVariables.akr_nginx_key)

        if set(AKRVariables.akr_php).issubset(set(apt_packages)):
            Log.info(self, "Adding repository for PHP, please wait...")
            # Add repository for php
            if AKRVariables.akr_platform_distro == 'debian':
                if AKRVariables.akr_platform_codename != 'jessie':
                    Log.debug(self, 'Adding repo_url of php for debian')
                    AKRRepo.add(self, repo_url=AKRVariables.akr_php_repo)
                    Log.debug(self, 'Adding Dotdeb/php GPG key')
                    AKRRepo.add_key(self, '89DF5277')
            else:
                Log.debug(self, 'Adding ppa for PHP')
                AKRRepo.add(self, ppa=AKRVariables.akr_php_repo)

        if set(AKRVariables.akr_hhvm).issubset(set(apt_packages)):
            Log.info(self, "Adding repository for HHVM, please wait...")
            if AKRVariables.akr_platform_codename == 'precise':
                Log.debug(self, 'Adding PPA for Boost')
                AKRRepo.add(self, ppa=AKRVariables.akr_boost_repo)
            Log.debug(self, 'Adding ppa repo for HHVM')
            AKRRepo.add(self, repo_url=AKRVariables.akr_hhvm_repo)
            Log.debug(self, 'Adding HHVM GPG Key')
            AKRRepo.add_key(self, '0x5a16e7281be7a449')

        if set(AKRVariables.akr_mail).issubset(set(apt_packages)):
            Log.debug(self, 'Executing the command debconf-set-selections.')
            try:
                AKRShellExec.cmd_exec(self, "echo \"dovecot-core dovecot-core/"
                                     "create-ssl-cert boolean yes\" "
                                     "| debconf-set-selections")
                AKRShellExec.cmd_exec(self, "echo \"dovecot-core dovecot-core"
                                     "/ssl-cert-name string $(hostname -f)\""
                                     " | debconf-set-selections")
            except CommandExecutionError as e:
                Log.error("Failed to initialize dovecot packages")

        if set(AKRVariables.akr_redis).issubset(set(apt_packages)):
            Log.info(self, "Adding repository for Redis, please wait...")
            if AKRVariables.akr_platform_distro == 'debian':
                Log.debug(self, 'Adding repo_url of redis for debian')
                AKRRepo.add(self, repo_url=AKRVariables.akr_redis_repo)
                Log.debug(self, 'Adding Dotdeb GPG key')
                AKRRepo.add_key(self, '89DF5277')
            else:
                Log.debug(self, 'Adding ppa for redis')
                AKRRepo.add(self, ppa=AKRVariables.akr_redis_repo)

    @expose(hide=True)
    def post_pref(self, apt_packages, packages):
        """Post activity after installation of packages"""
        if len(apt_packages):
            if set(AKRVariables.akr_postfix).issubset(set(apt_packages)):
                AKRGit.add(self, ["/etc/postfix"],
                          msg="Adding Postfix into Git")
                AKRService.reload_service(self, 'postfix')

            if set(AKRVariables.akr_nginx).issubset(set(apt_packages)) or set(AKRVariables.akr_nginx_dev)\
                                                                                .issubset(set(apt_packages)) :
                if set(["nginx-plus"]).issubset(set(apt_packages)):
                    # Fix for white screen death with NGINX PLUS
                    if not AKRFileUtils.grep(self, '/etc/nginx/fastcgi_params',
                                            'SCRIPT_FILENAME'):
                        with open('/etc/nginx/fastcgi_params', encoding='utf-8',
                                  mode='a') as akr_nginx:
                            akr_nginx.write('fastcgi_param \tSCRIPT_FILENAME '
                                           '\t$request_filename;\n')

                if os.path.isfile('/etc/nginx/sites-available/22222'):
                    http2 = "http2" if AKRAptGet.is_installed(self,'nginx-mainline') else "spdy"
                    if not AKRShellExec.cmd_exec(self, "grep  -q \'{http2}\' /etc/nginx/sites-available/22222".format(http2=http2)):
                            Log.debug(self, 'Setting http2/spdy in 22222')
                            AKRShellExec.cmd_exec(self, "sed -i 's/http2\|spdy/{0}/g' /etc/nginx/sites-available/22222".format(http2))

                sites = getAllsites(self)
                if sites:
                    for site in sites:
                        site_name = site.sitename
                        siteinfo = getSiteInfo(self, site_name)
                        ssl = ("enabled" if siteinfo.is_ssl else "disabled")
                        if (ssl == "enabled"):
                            if os.path.isfile('/var/www/{0}/conf/nginx/ssl.conf'.format(site_name)):
                                http2 =("http2" if AKRAptGet.is_installed(self,'nginx-mainline') else "spdy")
                                if not AKRShellExec.cmd_exec(self, "grep  -q \'{http2}\' /var/www/{site}/conf/nginx/ssl.conf".format(http2=http2,site=site_name)):
                                    Log.debug(self, 'Modifying http2/spdy parameter in /var/www/{0}/conf/nginx/ssl.conf'.format(site_name))
                                    AKRShellExec.cmd_exec(self, "sed -i 's/http2\|spdy/{http2}/g' /var/www/{site}/conf/nginx/ssl.conf".format(http2=http2,site=site_name))


                if not (os.path.isfile('/etc/nginx/common/ikfc.conf')):
                    # Change Akur8Engine Version in nginx.conf file
                    AKRFileUtils.searchreplace(self, "/etc/nginx/nginx.conf",
                                              "# add_header",
                                              "add_header")

                    AKRFileUtils.searchreplace(self, "/etc/nginx/nginx.conf",
                                              "\"Akur8Engine\"",
                                              "\"Akur8Engine {0}\""
                                              .format(AKRVariables.akr_version))
                    data = dict()
                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/conf.d/blockips.conf')
                    akr_nginx = open('/etc/nginx/conf.d/blockips.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'blockips.mustache', out=akr_nginx)
                    akr_nginx.close()

                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/conf.d/fastcgi.conf')
                    akr_nginx = open('/etc/nginx/conf.d/fastcgi.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'fastcgi.mustache', out=akr_nginx)
                    akr_nginx.close()

                    data = dict(php="9000", debug="9001", hhvm="8000",
                                hhvmconf=False)
                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/conf.d/upstream.conf')
                    akr_nginx = open('/etc/nginx/conf.d/upstream.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'upstream.mustache', out=akr_nginx)
                    akr_nginx.close()

                    # Setup Nginx common directory
                    if not os.path.exists('/etc/nginx/common'):
                        Log.debug(self, 'Creating directory'
                                  '/etc/nginx/common')
                        os.makedirs('/etc/nginx/common')

                    http2 = ("http2" if set(["nginx-mainline"]).issubset(set(apt_packages)) else "spdy")
                    data = dict(webroot=AKRVariables.akr_webroot,http2=http2)
                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/acl.conf')
                    akr_nginx = open('/etc/nginx/common/acl.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'acl.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/locations.conf')
                    akr_nginx = open('/etc/nginx/common/locations.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'locations.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/php.conf')
                    akr_nginx = open('/etc/nginx/common/php.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'php.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/w3tc.conf')
                    akr_nginx = open('/etc/nginx/common/w3tc.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'w3tc.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/ikcommon.conf')
                    akr_nginx = open('/etc/nginx/common/ikcommon.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'ikcommon.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/ikfc.conf')
                    akr_nginx = open('/etc/nginx/common/ikfc.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'ikfc.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/iksc.conf')
                    akr_nginx = open('/etc/nginx/common/iksc.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'iksc.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/iksubdir.conf')
                    akr_nginx = open('/etc/nginx/common/iksubdir.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'iksubdir.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                    # Nginx-Plus does not have nginx package structure like this
                    # So creating directories
                    if set(["nginx-plus"]).issubset(set(apt_packages)):
                        Log.info(self,
                                 "Installing Akur8Engine Configurations for" "NGINX PLUS")
                        if not os.path.exists('/etc/nginx/sites-available'):
                            Log.debug(self, 'Creating directory'
                                      '/etc/nginx/sites-available')
                            os.makedirs('/etc/nginx/sites-available')

                        if not os.path.exists('/etc/nginx/sites-enabled'):
                            Log.debug(self, 'Creating directory'
                                      '/etc/nginx/sites-available')
                            os.makedirs('/etc/nginx/sites-enabled')



                    # 22222 port settings
                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/sites-available/'
                              '22222')
                    akr_nginx = open('/etc/nginx/sites-available/22222',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), '22222.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                    passwd = ''.join([random.choice
                                     (string.ascii_letters + string.digits)
                                     for n in range(6)])
                    try:
                        AKRShellExec.cmd_exec(self, "printf \"akur8engine:"
                                             "$(openssl passwd -crypt "
                                             "{password} 2> /dev/null)\n\""
                                             "> /etc/nginx/htpasswd-akr "
                                             "2>/dev/null"
                                             .format(password=passwd))
                    except CommandExecutionError as e:
                        Log.error(self, "Failed to save HTTP Auth")

                    # Create Symbolic link for 22222
                    AKRFileUtils.create_symlink(self, ['/etc/nginx/'
                                                      'sites-available/'
                                                      '22222',
                                                      '/etc/nginx/'
                                                      'sites-enabled/'
                                                      '22222'])
                    # Create log and cert folder and softlinks
                    if not os.path.exists('{0}22222/logs'
                                          .format(AKRVariables.akr_webroot)):
                        Log.debug(self, "Creating directory "
                                  "{0}22222/logs "
                                  .format(AKRVariables.akr_webroot))
                        os.makedirs('{0}22222/logs'
                                    .format(AKRVariables.akr_webroot))

                    if not os.path.exists('{0}22222/cert'
                                          .format(AKRVariables.akr_webroot)):
                        Log.debug(self, "Creating directory "
                                  "{0}22222/cert"
                                  .format(AKRVariables.akr_webroot))
                        os.makedirs('{0}22222/cert'
                                    .format(AKRVariables.akr_webroot))

                    AKRFileUtils.create_symlink(self, ['/var/log/nginx/'
                                                      '22222.access.log',
                                                      '{0}22222/'
                                                      'logs/access.log'
                                               .format(AKRVariables.akr_webroot)]
                                               )

                    AKRFileUtils.create_symlink(self, ['/var/log/nginx/'
                                                      '22222.error.log',
                                                      '{0}22222/'
                                                      'logs/error.log'
                                               .format(AKRVariables.akr_webroot)]
                                               )

                    try:
                        AKRShellExec.cmd_exec(self, "openssl genrsa -out "
                                             "{0}22222/cert/22222.key 2048"
                                             .format(AKRVariables.akr_webroot))
                        AKRShellExec.cmd_exec(self, "openssl req -new -batch  "
                                             "-subj /commonName=127.0.0.1/ "
                                             "-key {0}22222/cert/22222.key "
                                             "-out {0}22222/cert/"
                                             "22222.csr"
                                             .format(AKRVariables.akr_webroot))

                        AKRFileUtils.mvfile(self, "{0}22222/cert/22222.key"
                                           .format(AKRVariables.akr_webroot),
                                           "{0}22222/cert/"
                                           "22222.key.org"
                                           .format(AKRVariables.akr_webroot))

                        AKRShellExec.cmd_exec(self, "openssl rsa -in "
                                             "{0}22222/cert/"
                                             "22222.key.org -out "
                                             "{0}22222/cert/22222.key"
                                             .format(AKRVariables.akr_webroot))

                        AKRShellExec.cmd_exec(self, "openssl x509 -req -days "
                                             "3652 -in {0}22222/cert/"
                                             "22222.csr -signkey {0}"
                                             "22222/cert/22222.key -out "
                                             "{0}22222/cert/22222.crt"
                                             .format(AKRVariables.akr_webroot))

                    except CommandExecutionError as e:
                        Log.error(self, "Failed to generate SSL for 22222")

                    # Nginx Configation into GIT
                    AKRGit.add(self,
                              ["/etc/nginx"], msg="Adding Nginx into Git")
                    AKRService.reload_service(self, 'nginx')
                    if set(["nginx-plus"]).issubset(set(apt_packages)):
                        AKRShellExec.cmd_exec(self, "sed -i -e 's/^user/#user/'"
                                            " -e '/^#user/a user"
                                            "\ www-data\;'"
                                            " /etc/nginx/nginx.conf")
                        if not AKRShellExec.cmd_exec(self, "cat /etc/nginx/"
                                                "nginx.conf | grep -q "
                                                "'/etc/nginx/sites-enabled'"):
                            AKRShellExec.cmd_exec(self, "sed -i '/\/etc\/"
                                                 "nginx\/conf\.d\/\*"
                                                 "\.conf/a \    include"
                                                 "\ \/etc\/nginx\/sites-enabled"
                                                 "\/*;' /etc/nginx/nginx.conf")

                        # Akur8Engine config for NGINX plus
                        data['version'] = AKRVariables.akr_version
                        Log.debug(self, 'Writting for nginx plus configuration'
                                  ' to file /etc/nginx/conf.d/akr-plus.conf')
                        akr_nginx = open('/etc/nginx/conf.d/akr-plus.conf',
                                        encoding='utf-8', mode='w')
                        self.app.render((data), 'akr-plus.mustache',
                                        out=akr_nginx)
                        akr_nginx.close()

                        print("HTTP Auth User Name: akur8engine"
                                    + "\nHTTP Auth Password : {0}".format(passwd))
                        AKRService.reload_service(self, 'nginx')
                    else:
                        self.msg = (self.msg + ["HTTP Auth User Name: akur8engine"]
                                + ["HTTP Auth Password : {0}".format(passwd)])
                else:
                    AKRService.restart_service(self, 'nginx')

                if AKRAptGet.is_installed(self,'redis-server'):
                    if os.path.isfile("/etc/nginx/nginx.conf") and (not
                       os.path.isfile("/etc/nginx/common/redis.conf")):

                        data = dict()
                        Log.debug(self, 'Writting the nginx configuration to '
                                  'file /etc/nginx/common/redis.conf')
                        akr_nginx = open('/etc/nginx/common/redis.conf',
                                        encoding='utf-8', mode='w')
                        self.app.render((data), 'redis.mustache',
                                        out=akr_nginx)
                        akr_nginx.close()

                    if os.path.isfile("/etc/nginx/nginx.conf") and (not
                       os.path.isfile("/etc/nginx/common/redis-hhvm.conf")):

                        data = dict()
                        Log.debug(self, 'Writting the nginx configuration to '
                                  'file /etc/nginx/common/redis-hhvm.conf')
                        akr_nginx = open('/etc/nginx/common/redis-hhvm.conf',
                                        encoding='utf-8', mode='w')
                        self.app.render((data), 'redis-hhvm.mustache',
                                        out=akr_nginx)
                        akr_nginx.close()

                    if os.path.isfile("/etc/nginx/conf.d/upstream.conf"):
                        if not AKRFileUtils.grep(self, "/etc/nginx/conf.d/"
                                                "upstream.conf",
                                                "redis"):
                            with open("/etc/nginx/conf.d/upstream.conf",
                                      "a") as redis_file:
                                redis_file.write("upstream redis {\n"
                                                 "    server 127.0.0.1:6379;\n"
                                                 "    keepalive 10;\n}\n")

                    if os.path.isfile("/etc/nginx/nginx.conf") and (not
                       os.path.isfile("/etc/nginx/conf.d/redis.conf")):
                        with open("/etc/nginx/conf.d/redis.conf", "a") as redis_file:
                            redis_file.write("# Log format Settings\n"
                                             "log_format rt_cache_redis '$remote_addr $upstream_response_time $srcache_fetch_status [$time_local] '\n"
                                             "'$http_host \"$request\" $status $body_bytes_sent '\n"
                                             "'\"$http_referer\" \"$http_user_agent\"';\n")

            # Set up pagespeed config
            if self.app.pargs.pagespeed:
                if (os.path.isfile('/etc/nginx/nginx.conf') and
                    (not os.path.isfile('/etc/nginx/conf.d/pagespeed.conf'))):
                    # Pagespeed configuration
                    data = dict()
                    Log.debug(self, 'Writting the Pagespeed Global '
                              'configuration to file /etc/nginx/conf.d/'
                              'pagespeed.conf')
                    akr_nginx = open('/etc/nginx/conf.d/pagespeed.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'pagespeed-global.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

            if set(AKRVariables.akr_hhvm).issubset(set(apt_packages)):

                AKRShellExec.cmd_exec(self, "update-rc.d hhvm defaults")

                AKRFileUtils.searchreplace(self, "/etc/hhvm/server.ini",
                                                "9000", "8000")
                AKRFileUtils.searchreplace(self, "/etc/nginx/hhvm.conf",
                                                "9000", "8000")

                with open("/etc/hhvm/php.ini", "a") as hhvm_file:
                    hhvm_file.write("hhvm.log.header = true\n"
                                    "hhvm.log.natives_stack_trace = true\n"
                                    "hhvm.mysql.socket = "
                                    "/var/run/mysqld/mysqld.sock\n"
                                    "hhvm.pdo_mysql.socket = "
                                    "/var/run/mysqld/mysqld.sock\n"
                                    "hhvm.mysqli.socket = "
                                    "/var/run/mysqld/mysqld.sock\n")

                with open("/etc/hhvm/server.ini", "a") as hhvm_file:
                    hhvm_file.write("hhvm.server.ip = 127.0.0.1\n")

                if os.path.isfile("/etc/nginx/conf.d/fastcgi.conf"):
                    if not AKRFileUtils.grep(self, "/etc/nginx/conf.d/"
                                            "fastcgi.conf",
                                            "fastcgi_keep_conn"):
                        with open("/etc/nginx/conf.d/fastcgi.conf",
                                  "a") as hhvm_file:
                            hhvm_file.write("fastcgi_keep_conn on;\n")

                if os.path.isfile("/etc/nginx/conf.d/upstream.conf"):
                    if not AKRFileUtils.grep(self, "/etc/nginx/conf.d/"
                                            "upstream.conf",
                                            "hhvm"):
                        with open("/etc/nginx/conf.d/upstream.conf",
                                  "a") as hhvm_file:
                            hhvm_file.write("upstream hhvm {\nserver "
                                            "127.0.0.1:8000;\n"
                                            "server 127.0.0.1:9000 backup;\n}"
                                            "\n")

                AKRGit.add(self, ["/etc/hhvm"], msg="Adding HHVM into Git")
                AKRService.restart_service(self, 'hhvm')

                if os.path.isfile("/etc/nginx/nginx.conf") and (not
                   os.path.isfile("/etc/nginx/common/php-hhvm.conf")):

                    data = dict()
                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/php-hhvm.conf')
                    akr_nginx = open('/etc/nginx/common/php-hhvm.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'php-hhvm.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/w3tc-hhvm.conf')
                    akr_nginx = open('/etc/nginx/common/w3tc-hhvm.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'w3tc-hhvm.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/ikfc-hhvm.conf')
                    akr_nginx = open('/etc/nginx/common/ikfc-hhvm.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'ikfc-hhvm.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/iksc-hhvm.conf')
                    akr_nginx = open('/etc/nginx/common/iksc-hhvm.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'iksc-hhvm.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                    if not AKRService.reload_service(self, 'nginx'):
                        Log.error(self, "Failed to reload Nginx, please check "
                                        "output of `nginx -t`")

            if set(AKRVariables.akr_redis).issubset(set(apt_packages)):
                if os.path.isfile("/etc/nginx/nginx.conf") and (not
                   os.path.isfile("/etc/nginx/common/redis.conf")):

                    data = dict()
                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/redis.conf')
                    akr_nginx = open('/etc/nginx/common/redis.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'redis.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                if os.path.isfile("/etc/nginx/nginx.conf") and (not
                   os.path.isfile("/etc/nginx/common/redis-hhvm.conf")):

                    data = dict()
                    Log.debug(self, 'Writting the nginx configuration to '
                              'file /etc/nginx/common/redis-hhvm.conf')
                    akr_nginx = open('/etc/nginx/common/redis-hhvm.conf',
                                    encoding='utf-8', mode='w')
                    self.app.render((data), 'redis-hhvm.mustache',
                                    out=akr_nginx)
                    akr_nginx.close()

                if os.path.isfile("/etc/nginx/conf.d/upstream.conf"):
                    if not AKRFileUtils.grep(self, "/etc/nginx/conf.d/"
                                            "upstream.conf",
                                            "redis"):
                        with open("/etc/nginx/conf.d/upstream.conf",
                                  "a") as redis_file:
                            redis_file.write("upstream redis {\n"
                                             "    server 127.0.0.1:6379;\n"
                                             "    keepalive 10;\n}\n")

                if os.path.isfile("/etc/nginx/nginx.conf") and (not
                   os.path.isfile("/etc/nginx/conf.d/redis.conf")):
                    with open("/etc/nginx/conf.d/redis.conf", "a") as redis_file:
                        redis_file.write("# Log format Settings\n"
                                         "log_format rt_cache_redis '$remote_addr $upstream_response_time $srcache_fetch_status [$time_local] '\n"
                                         "'$http_host \"$request\" $status $body_bytes_sent '\n"
                                         "'\"$http_referer\" \"$http_user_agent\"';\n")

            if set(AKRVariables.akr_php).issubset(set(apt_packages)):
                # Create log directories
                if not os.path.exists('/var/log/php5/'):
                    Log.debug(self, 'Creating directory /var/log/php5/')
                    os.makedirs('/var/log/php5/')

                # For debian install xdebug

                if (AKRVariables.akr_platform_distro == "debian" and
                   AKRVariables.akr_platform_codename == 'wheezy'):
                    AKRShellExec.cmd_exec(self, "pecl install xdebug")

                    with open("/etc/php5/mods-available/xdebug.ini",
                              encoding='utf-8', mode='a') as myfile:
                        myfile.write("zend_extension=/usr/lib/php5/20131226/"
                                     "xdebug.so\n")

                    AKRFileUtils.create_symlink(self, ["/etc/php5/"
                                               "mods-available/xdebug.ini",
                                                      "/etc/php5/fpm/conf.d"
                                                      "/20-xedbug.ini"])

                # Parse etc/php5/fpm/php.ini
                config = configparser.ConfigParser()
                Log.debug(self, "configuring php file /etc/php5/fpm/php.ini")
                config.read('/etc/php5/fpm/php.ini')
                config['PHP']['expose_php'] = 'Off'
                config['PHP']['post_max_size'] = '100M'
                config['PHP']['upload_max_filesize'] = '100M'
                config['PHP']['max_execution_time'] = '300'
                config['PHP']['date.timezone'] = AKRVariables.akr_timezone
                with open('/etc/php5/fpm/php.ini',
                          encoding='utf-8', mode='w') as configfile:
                    Log.debug(self, "Writting php configuration into "
                              "/etc/php5/fpm/php.ini")
                    config.write(configfile)

                # Prase /etc/php5/fpm/php-fpm.conf
                config = configparser.ConfigParser()
                Log.debug(self, "configuring php file"
                          "/etc/php5/fpm/php-fpm.conf")
                config.read_file(codecs.open("/etc/php5/fpm/php-fpm.conf",
                                             "r", "utf8"))
                config['global']['error_log'] = '/var/log/php5/fpm.log'
                config.remove_option('global', 'include')
                config['global']['log_level'] = 'notice'
                config['global']['include'] = '/etc/php5/fpm/pool.d/*.conf'
                with codecs.open('/etc/php5/fpm/php-fpm.conf',
                                 encoding='utf-8', mode='w') as configfile:
                    Log.debug(self, "writting php5 configuration into "
                              "/etc/php5/fpm/php-fpm.conf")
                    config.write(configfile)

                # Parse /etc/php5/fpm/pool.d/www.conf
                config = configparser.ConfigParser()
                config.read_file(codecs.open('/etc/php5/fpm/pool.d/www.conf',
                                             "r", "utf8"))
                config['www']['ping.path'] = '/ping'
                config['www']['pm.status_path'] = '/status'
                config['www']['pm.max_requests'] = '500'
                config['www']['pm.max_children'] = '100'
                config['www']['pm.start_servers'] = '20'
                config['www']['pm.min_spare_servers'] = '10'
                config['www']['pm.max_spare_servers'] = '30'
                config['www']['request_terminate_timeout'] = '300'
                config['www']['pm'] = 'ondemand'
                config['www']['listen'] = '127.0.0.1:9000'
                with codecs.open('/etc/php5/fpm/pool.d/www.conf',
                                 encoding='utf-8', mode='w') as configfile:
                    Log.debug(self, "writting PHP5 configuration into "
                              "/etc/php5/fpm/pool.d/www.conf")
                    config.write(configfile)

                # Generate /etc/php5/fpm/pool.d/debug.conf
                AKRFileUtils.copyfile(self, "/etc/php5/fpm/pool.d/www.conf",
                                     "/etc/php5/fpm/pool.d/debug.conf")
                AKRFileUtils.searchreplace(self, "/etc/php5/fpm/pool.d/"
                                          "debug.conf", "[www]", "[debug]")
                config = configparser.ConfigParser()
                config.read('/etc/php5/fpm/pool.d/debug.conf')
                config['debug']['listen'] = '127.0.0.1:9001'
                config['debug']['rlimit_core'] = 'unlimited'
                config['debug']['slowlog'] = '/var/log/php5/slow.log'
                config['debug']['request_slowlog_timeout'] = '10s'
                with open('/etc/php5/fpm/pool.d/debug.conf',
                          encoding='utf-8', mode='w') as confifile:
                    Log.debug(self, "writting PHP5 configuration into "
                              "/etc/php5/fpm/pool.d/debug.conf")
                    config.write(confifile)

                with open("/etc/php5/fpm/pool.d/debug.conf",
                          encoding='utf-8', mode='a') as myfile:
                    myfile.write("php_admin_value[xdebug.profiler_output_dir] "
                                 "= /tmp/ \nphp_admin_value[xdebug.profiler_"
                                 "output_name] = cachegrind.out.%p-%H-%R "
                                 "\nphp_admin_flag[xdebug.profiler_enable"
                                 "_trigger] = on \nphp_admin_flag[xdebug."
                                 "profiler_enable] = off\n")

                # Disable xdebug
                AKRFileUtils.searchreplace(self, "/etc/php5/mods-available/"
                                          "xdebug.ini",
                                          "zend_extension",
                                          ";zend_extension")

                # PHP and Debug pull configuration
                if not os.path.exists('{0}22222/htdocs/fpm/status/'
                                      .format(AKRVariables.akr_webroot)):
                    Log.debug(self, 'Creating directory '
                              '{0}22222/htdocs/fpm/status/ '
                              .format(AKRVariables.akr_webroot))
                    os.makedirs('{0}22222/htdocs/fpm/status/'
                                .format(AKRVariables.akr_webroot))
                open('{0}22222/htdocs/fpm/status/debug'
                     .format(AKRVariables.akr_webroot),
                     encoding='utf-8', mode='a').close()
                open('{0}22222/htdocs/fpm/status/php'
                     .format(AKRVariables.akr_webroot),
                     encoding='utf-8', mode='a').close()

                # Write info.php
                if not os.path.exists('{0}22222/htdocs/php/'
                                      .format(AKRVariables.akr_webroot)):
                    Log.debug(self, 'Creating directory '
                              '{0}22222/htdocs/php/ '
                              .format(AKRVariables.akr_webroot))
                    os.makedirs('{0}22222/htdocs/php'
                                .format(AKRVariables.akr_webroot))

                with open("{0}22222/htdocs/php/info.php"
                          .format(AKRVariables.akr_webroot),
                          encoding='utf-8', mode='w') as myfile:
                    myfile.write("<?php\nphpinfo();\n?>")

                AKRFileUtils.chown(self, "{0}22222"
                                  .format(AKRVariables.akr_webroot),
                                  AKRVariables.akr_php_user,
                                  AKRVariables.akr_php_user, recursive=True)

                AKRGit.add(self, ["/etc/php5"], msg="Adding PHP into Git")
                AKRService.restart_service(self, 'php5-fpm')

            if set(AKRVariables.akr_mysql).issubset(set(apt_packages)):
                # TODO: Currently we are using, we need to remove it in future
                # config = configparser.ConfigParser()
                # config.read('/etc/mysql/my.cnf')
                # config['mysqld']['wait_timeout'] = 30
                # config['mysqld']['interactive_timeout'] = 60
                # config['mysqld']['performance_schema'] = 0
                # with open('/etc/mysql/my.cnf', 'w') as configfile:
                #     config.write(configfile)
                if not os.path.isfile("/etc/mysql/my.cnf"):
                    config = ("[mysqld]\nwait_timeout = 30\n"
                              "interactive_timeout=60\nperformance_schema = 0"
                              "\nquery_cache_type = 1")
                    config_file = open("/etc/mysql/my.cnf",
                                       encoding='utf-8', mode='w')
                    config_file.write(config)
                    config_file.close()
                else:
                    try:
                        AKRShellExec.cmd_exec(self, "sed -i \"/#max_conn"
                                             "ections/a wait_timeout = 30 \\n"
                                             "interactive_timeout = 60 \\n"
                                             "performance_schema = 0\\n"
                                             "query_cache_type = 1 \" "
                                             "/etc/mysql/my.cnf")
                    except CommandExecutionError as e:
                        Log.error(self, "Unable to update MySQL file")

                # Set MySQLTuner permission
                AKRFileUtils.chmod(self, "/usr/bin/mysqltuner", 0o775)

                AKRGit.add(self, ["/etc/mysql"], msg="Adding MySQL into Git")
                AKRService.reload_service(self, 'mysql')

            if set(AKRVariables.akr_mail).issubset(set(apt_packages)):
                Log.debug(self, "Adding user")
                try:
                    AKRShellExec.cmd_exec(self, "adduser --uid 5000 --home /var"
                                         "/vmail --disabled-password --gecos "
                                         "'' vmail")
                except CommandExecutionError as e:
                    Log.error(self, "Unable to add vmail user for mail server")
                try:
                    AKRShellExec.cmd_exec(self, "openssl req -new -x509 -days"
                                         " 3650 "
                                         "-nodes -subj /commonName={hostname}"
                                         "/emailAddress={email} -out /etc/ssl"
                                         "/certs/dovecot."
                                         "pem -keyout "
                                         "/etc/ssl/private/dovecot.pem"
                                         .format(hostname=AKRVariables.akr_fqdn,
                                                 email=AKRVariables.akr_email))
                except CommandExecutionError as e:
                    Log.error(self, "Unable to generate PEM key for dovecot")
                Log.debug(self, "Setting Privileges to "
                          "/etc/ssl/private/dovecot.pem file ")
                AKRFileUtils.chmod(self, "/etc/ssl/private/dovecot.pem", 0o600)

                # Custom Dovecot configuration by Akur8Engine
                data = dict()
                Log.debug(self, "Writting configuration into file"
                          "/etc/dovecot/conf.d/auth-sql.conf.ext ")
                akr_dovecot = open('/etc/dovecot/conf.d/auth-sql.conf.ext',
                                  encoding='utf-8', mode='w')
                self.app.render((data), 'auth-sql-conf.mustache',
                                out=akr_dovecot)
                akr_dovecot.close()

                data = dict(email=AKRVariables.akr_email)
                Log.debug(self, "Writting configuration into file"
                          "/etc/dovecot/conf.d/99-akr.conf ")
                akr_dovecot = open('/etc/dovecot/conf.d/99-akr.conf',
                                  encoding='utf-8', mode='w')
                self.app.render((data), 'dovecot.mustache', out=akr_dovecot)
                akr_dovecot.close()
                try:
                    AKRShellExec.cmd_exec(self, "sed -i \"s/\\!include "
                                         "auth-system.conf.ext/#\\!include "
                                         "auth-system.conf.ext/\" "
                                         "/etc/dovecot/conf.d/10-auth.conf")

                    AKRShellExec.cmd_exec(self, "sed -i \"s\'/etc/dovecot/"
                                         "dovecot.pem\'/etc/ssl/certs/"
                                         "dovecot.pem"
                                         "\'\" /etc/dovecot/conf.d/"
                                         "10-ssl.conf")
                    AKRShellExec.cmd_exec(self, "sed -i \"s\'/etc/dovecot/"
                                         "private/dovecot.pem\'/etc/ssl/"
                                         "private"
                                         "/dovecot.pem\'\" /etc/dovecot/"
                                         "conf.d/"
                                         "10-ssl.conf")

                    # Custom Postfix configuration needed with Dovecot
                    # Changes in master.cf
                    # TODO: Find alternative for sed in Python
                    AKRShellExec.cmd_exec(self, "sed -i \'s/#submission/"
                                         "submission/\'"
                                         " /etc/postfix/master.cf")
                    AKRShellExec.cmd_exec(self, "sed -i \'s/#smtps/smtps/\'"
                                         " /etc/postfix/master.cf")

                    AKRShellExec.cmd_exec(self, "postconf -e \"smtpd_sasl_type "
                                         "= dovecot\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \"smtpd_sasl_path "
                                         "= private/auth\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \""
                                         "smtpd_sasl_auth_enable = "
                                         "yes\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \""
                                         " smtpd_relay_restrictions ="
                                         " permit_sasl_authenticated, "
                                         " permit_mynetworks, "
                                         " reject_unauth_destination\"")

                    AKRShellExec.cmd_exec(self, "postconf -e \""
                                         "smtpd_tls_mandatory_"
                                         "protocols = !SSLv2,!SSLv3\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \"smtp_tls_"
                                         "mandatory_protocols = !SSLv2,"
                                         "!SSLv3\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \"smtpd_tls"
                                         "_protocols = !SSLv2,!SSLv3\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \"smtp_tls"
                                         "_protocols = !SSLv2,!SSLv3\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \"mydestination "
                                         "= localhost\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \"virtual"
                                         "_transport "
                                         "= lmtp:unix:private/dovecot-lmtp\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \"virtual_uid_"
                                         "maps = static:5000\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \"virtual_gid_"
                                         "maps = static:5000\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \""
                                         " virtual_mailbox_domains = "
                                         "mysql:/etc/postfix/mysql/virtual_"
                                         "domains_maps.cf\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \"virtual_mailbox"
                                         "_maps"
                                         " = mysql:/etc/postfix/mysql/virtual_"
                                         "mailbox_maps.cf\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \"virtual_alias"
                                         "_maps  "
                                         "= mysql:/etc/postfix/mysql/virtual_"
                                         "alias_maps.cf\"")
                    AKRShellExec.cmd_exec(self, "openssl req -new -x509 -days "
                                         " 3650 -nodes -subj /commonName="
                                         "{hostname}/emailAddress={email}"
                                         " -out /etc/ssl/certs/postfix.pem"
                                         " -keyout /etc/ssl/private/"
                                         "postfix.pem"
                                         .format(hostname=AKRVariables.akr_fqdn,
                                                 email=AKRVariables.akr_email))
                    AKRShellExec.cmd_exec(self, "chmod 0600 /etc/ssl/private"
                                         "/postfix.pem")
                    AKRShellExec.cmd_exec(self, "postconf -e \"smtpd_tls_cert_"
                                         "file = /etc/ssl/certs/postfix.pem\"")
                    AKRShellExec.cmd_exec(self, "postconf -e \"smtpd_tls_key_"
                                         "file = /etc/ssl/private/"
                                         "postfix.pem\"")

                except CommandExecutionError as e:
                    Log.Error(self, "Failed to update Dovecot configuration")

                # Sieve configuration
                if not os.path.exists('/var/lib/dovecot/sieve/'):
                    Log.debug(self, 'Creating directory '
                              '/var/lib/dovecot/sieve/ ')
                    os.makedirs('/var/lib/dovecot/sieve/')

                # Custom sieve configuration by Akur8Engine
                data = dict()
                Log.debug(self, "Writting configuration of Akur8Engine into "
                          "file /var/lib/dovecot/sieve/default.sieve")
                akr_sieve = open('/var/lib/dovecot/sieve/default.sieve',
                                encoding='utf-8', mode='w')
                self.app.render((data), 'default-sieve.mustache',
                                out=akr_sieve)
                akr_sieve.close()

                # Compile sieve rules
                Log.debug(self, "Setting Privileges to dovecot ")
                # AKRShellExec.cmd_exec(self, "chown -R vmail:vmail /var/lib"
                #                     "/dovecot")
                AKRFileUtils.chown(self, "/var/lib/dovecot", 'vmail', 'vmail',
                                  recursive=True)
                try:
                    AKRShellExec.cmd_exec(self, "sievec /var/lib/dovecot/"
                                         "/sieve/default.sieve")
                except CommandExecutionError as e:
                    raise SiteError("Failed to compile default.sieve")

                AKRGit.add(self, ["/etc/postfix", "/etc/dovecot"],
                          msg="Installed mail server")
                AKRService.restart_service(self, 'dovecot')
                AKRService.reload_service(self, 'postfix')

            if set(AKRVariables.akr_mailscanner).issubset(set(apt_packages)):
                # Set up Custom amavis configuration
                data = dict()
                Log.debug(self, "Configuring file /etc/amavis/conf.d"
                          "/15-content_filter_mode")
                akr_amavis = open('/etc/amavis/conf.d/15-content_filter_mode',
                                 encoding='utf-8', mode='w')
                self.app.render((data), '15-content_filter_mode.mustache',
                                out=akr_amavis)
                akr_amavis.close()

                # Amavis ViMbadmin configuration
                if os.path.isfile("/etc/postfix/mysql/virtual_alias_maps.cf"):
                    vm_host = os.popen("grep hosts /etc/postfix/mysql/virtual_"
                                       "alias_maps.cf | awk \'{ print $3 }\' |"
                                       " tr -d '\\n'").read()
                    vm_pass = os.popen("grep password /etc/postfix/mysql/"
                                       "virtual_alias_maps.cf | awk \'{ print "
                                       "$3 }\' | tr -d '\\n'").read()

                    data = dict(host=vm_host, password=vm_pass)
                    vm_config = open('/etc/amavis/conf.d/50-user',
                                     encoding='utf-8', mode='w')
                    self.app.render((data), '50-user.mustache', out=vm_config)
                    vm_config.close()

                # Amavis postfix configuration
                try:
                    AKRShellExec.cmd_exec(self, "postconf -e \"content_filter ="
                                         " smtp-amavis:[127.0.0.1]:10024\"")
                    AKRShellExec.cmd_exec(self, "sed -i \"s/1       pickup/1   "
                                         "    pickup"
                                         "\\n        -o content_filter=\\n    "
                                         "  -o receive_override_options="
                                         "no_header_body"
                                         "_checks/\" /etc/postfix/master.cf")
                except CommandExecutionError as e:
                    raise SiteError("Failed to update Amavis-Postfix config")

                amavis_master = ("""smtp-amavis unix - - n - 2 smtp
    -o smtp_data_done_timeout=1200
    -o smtp_send_xforward_command=yes
    -o disable_dns_lookups=yes
    -o max_use=20
127.0.0.1:10025 inet n - n - - smtpd
    -o content_filter=
    -o smtpd_delay_reject=no
    -o smtpd_client_restrictions=permit_mynetworks,reject
    -o smtpd_helo_restrictions=
    -o smtpd_sender_restrictions=
    -o smtpd_recipient_restrictions=permit_mynetworks,reject
    -o smtpd_data_restrictions=reject_unauth_pipelining
    -o smtpd_end_of_data_restrictions=
    -o smtpd_restriction_classes=
    -o mynetworks=127.0.0.0/8
    -o smtpd_error_sleep_time=0
    -o smtpd_soft_error_limit=1001
    -o smtpd_hard_error_limit=1000
    -o smtpd_client_connection_count_limit=0
    -o smtpd_client_connection_rate_limit=0
    -o local_header_rewrite_clients=""")

                with open("/etc/postfix/master.cf",
                          encoding='utf-8', mode='a') as am_config:
                        am_config.write(amavis_master)

                try:
                    # Amavis ClamAV configuration
                    Log.debug(self, "Adding new user clamav amavis")
                    AKRShellExec.cmd_exec(self, "adduser clamav amavis")
                    Log.debug(self, "Adding new user amavis clamav")
                    AKRShellExec.cmd_exec(self, "adduser amavis clamav")
                    Log.debug(self, "Setting Privileges to /var/lib/amavis"
                              "/tmp")
                    AKRFileUtils.chmod(self, "/var/lib/amavis/tmp", 0o755)

                    # Update ClamAV database
                    Log.debug(self, "Updating database")
                    AKRShellExec.cmd_exec(self, "freshclam")
                except CommandExecutionError as e:
                    raise SiteError(" Unable to update ClamAV-Amavis config")

                AKRGit.add(self, ["/etc/amavis"], msg="Adding Amavis into Git")
                AKRService.restart_service(self, 'dovecot')
                AKRService.reload_service(self, 'postfix')
                AKRService.restart_service(self, 'amavis')

        if len(packages):
            if any('/usr/bin/ik' == x[1] for x in packages):
                Log.debug(self, "Setting Privileges to /usr/bin/ik file ")
                AKRFileUtils.chmod(self, "/usr/bin/ik", 0o775)

            if any('/tmp/pma.tar.gz' == x[1]
                    for x in packages):
                AKRExtract.extract(self, '/tmp/pma.tar.gz', '/tmp/')
                Log.debug(self, 'Extracting file /tmp/pma.tar.gz to '
                          'location /tmp/')
                if not os.path.exists('{0}22222/htdocs/db'
                                      .format(AKRVariables.akr_webroot)):
                    Log.debug(self, "Creating new  directory "
                              "{0}22222/htdocs/db"
                              .format(AKRVariables.akr_webroot))
                    os.makedirs('{0}22222/htdocs/db'
                                .format(AKRVariables.akr_webroot))
                shutil.move('/tmp/phpmyadmin-STABLE/',
                            '{0}22222/htdocs/db/pma/'
                            .format(AKRVariables.akr_webroot))
                shutil.copyfile('{0}22222/htdocs/db/pma/config.sample.inc.php'
                                .format(AKRVariables.akr_webroot),
                                '{0}22222/htdocs/db/pma/config.inc.php'
                                .format(AKRVariables.akr_webroot))
                Log.debug(self, 'Setting Blowfish Secret Key FOR COOKIE AUTH to  '
                          '{0}22222/htdocs/db/pma/config.inc.php file '
                          .format(AKRVariables.akr_webroot))
                blowfish_key = ''.join([random.choice
                         (string.ascii_letters + string.digits)
                         for n in range(10)])
                AKRFileUtils.searchreplace(self,
                                          '{0}22222/htdocs/db/pma/config.inc.php'
                                          .format(AKRVariables.akr_webroot),
                                          "$cfg[\'blowfish_secret\'] = \'\';","$cfg[\'blowfish_secret\'] = \'{0}\';"
                                          .format(blowfish_key))
                Log.debug(self, 'Setting HOST Server For Mysql to  '
                          '{0}22222/htdocs/db/pma/config.inc.php file '
                          .format(AKRVariables.akr_webroot))
                AKRFileUtils.searchreplace(self,
                                          '{0}22222/htdocs/db/pma/config.inc.php'
                                          .format(AKRVariables.akr_webroot),
                                          "$cfg[\'Servers\'][$i][\'host\'] = \'localhost\';","$cfg[\'Servers\'][$i][\'host\'] = \'{0}\';"
                                          .format(AKRVariables.akr_mysql_host))
                Log.debug(self, 'Setting Privileges of webroot permission to  '
                          '{0}22222/htdocs/db/pma file '
                          .format(AKRVariables.akr_webroot))
                AKRFileUtils.chown(self, '{0}22222'
                                  .format(AKRVariables.akr_webroot),
                                  AKRVariables.akr_php_user,
                                  AKRVariables.akr_php_user,
                                  recursive=True)
            if any('/tmp/memcache.tar.gz' == x[1]
                    for x in packages):
                Log.debug(self, "Extracting memcache.tar.gz to location"
                          " {0}22222/htdocs/cache/memcache "
                          .format(AKRVariables.akr_webroot))
                AKRExtract.extract(self, '/tmp/memcache.tar.gz',
                                  '{0}22222/htdocs/cache/memcache'
                                  .format(AKRVariables.akr_webroot))
                Log.debug(self, "Setting Privileges to "
                          "{0}22222/htdocs/cache/memcache file"
                          .format(AKRVariables.akr_webroot))
                AKRFileUtils.chown(self, '{0}22222'
                                  .format(AKRVariables.akr_webroot),
                                  AKRVariables.akr_php_user,
                                  AKRVariables.akr_php_user,
                                  recursive=True)

            if any('/tmp/webgrind.tar.gz' == x[1]
                    for x in packages):
                Log.debug(self, "Extracting file webgrind.tar.gz to "
                          "location /tmp/ ")
                AKRExtract.extract(self, '/tmp/webgrind.tar.gz', '/tmp/')
                if not os.path.exists('{0}22222/htdocs/php'
                                      .format(AKRVariables.akr_webroot)):
                    Log.debug(self, "Creating directroy "
                              "{0}22222/htdocs/php"
                              .format(AKRVariables.akr_webroot))
                    os.makedirs('{0}22222/htdocs/php'
                                .format(AKRVariables.akr_webroot))
                shutil.move('/tmp/webgrind-master/',
                            '{0}22222/htdocs/php/webgrind'
                            .format(AKRVariables.akr_webroot))

                AKRFileUtils.searchreplace(self, "{0}22222/htdocs/php/webgrind/"
                                          "config.php"
                                          .format(AKRVariables.akr_webroot),
                                          "/usr/local/bin/dot", "/usr/bin/dot")
                AKRFileUtils.searchreplace(self, "{0}22222/htdocs/php/webgrind/"
                                          "config.php"
                                          .format(AKRVariables.akr_webroot),
                                          "Europe/Copenhagen",
                                          AKRVariables.akr_timezone)

                AKRFileUtils.searchreplace(self, "{0}22222/htdocs/php/webgrind/"
                                          "config.php"
                                          .format(AKRVariables.akr_webroot),
                                          "90", "100")

                Log.debug(self, "Setting Privileges of webroot permission to "
                          "{0}22222/htdocs/php/webgrind/ file "
                          .format(AKRVariables.akr_webroot))
                AKRFileUtils.chown(self, '{0}22222'
                                  .format(AKRVariables.akr_webroot),
                                  AKRVariables.akr_php_user,
                                  AKRVariables.akr_php_user,
                                  recursive=True)

            if any('/tmp/anemometer.tar.gz' == x[1]
                    for x in packages):
                Log.debug(self, "Extracting file anemometer.tar.gz to "
                          "location /tmp/ ")
                AKRExtract.extract(self, '/tmp/anemometer.tar.gz', '/tmp/')
                if not os.path.exists('{0}22222/htdocs/db/'
                                      .format(AKRVariables.akr_webroot)):
                    Log.debug(self, "Creating directory")
                    os.makedirs('{0}22222/htdocs/db/'
                                .format(AKRVariables.akr_webroot))
                shutil.move('/tmp/Anemometer-master',
                            '{0}22222/htdocs/db/anemometer'
                            .format(AKRVariables.akr_webroot))
                chars = ''.join(random.sample(string.ascii_letters, 8))
                try:
                    AKRShellExec.cmd_exec(self, 'mysql < {0}22222/htdocs/db'
                                         '/anemometer/install.sql'
                                         .format(AKRVariables.akr_webroot))
                except CommandExecutionError as e:
                    raise SiteError("Unable to import Anemometer database")

                AKRMysql.execute(self, 'grant select on *.* to \'anemometer\''
                                '@\'{0}\' IDENTIFIED'
                                ' BY \'{1}\''.format(self.app.config.get('mysql',
                                                  'grant-host'),chars))
                Log.debug(self, "grant all on slow-query-log.*"
                          " to anemometer@root_user IDENTIFIED BY password ")
                AKRMysql.execute(self, 'grant all on slow_query_log.* to'
                                '\'anemometer\'@\'{0}\' IDENTIFIED'
                                ' BY \'{1}\''.format(self.app.config.get(
                                                     'mysql', 'grant-host'),
                                                     chars),
                                errormsg="cannot grant priviledges", log=False)

                # Custom Anemometer configuration
                Log.debug(self, "configration Anemometer")
                data = dict(host=AKRVariables.akr_mysql_host, port='3306',
                            user='anemometer', password=chars)
                akr_anemometer = open('{0}22222/htdocs/db/anemometer'
                                     '/conf/config.inc.php'
                                     .format(AKRVariables.akr_webroot),
                                     encoding='utf-8', mode='w')
                self.app.render((data), 'anemometer.mustache',
                                out=akr_anemometer)
                akr_anemometer.close()

            if any('/usr/bin/pt-query-advisor' == x[1]
                    for x in packages):
                AKRFileUtils.chmod(self, "/usr/bin/pt-query-advisor", 0o775)

            if any('/tmp/vimbadmin.tar.gz' == x[1] for x in packages):
                # Extract ViMbAdmin
                Log.debug(self, "Extracting ViMbAdmin.tar.gz to "
                          "location /tmp/")
                AKRExtract.extract(self, '/tmp/vimbadmin.tar.gz', '/tmp/')
                if not os.path.exists('{0}22222/htdocs/'
                                      .format(AKRVariables.akr_webroot)):
                    Log.debug(self, "Creating directory "
                              "{0}22222/htdocs/"
                              .format(AKRVariables.akr_webroot))
                    os.makedirs('{0}22222/htdocs/'
                                .format(AKRVariables.akr_webroot))
                shutil.move('/tmp/ViMbAdmin-{0}/'
                            .format(AKRVariables.akr_vimbadmin),
                            '{0}22222/htdocs/vimbadmin/'
                            .format(AKRVariables.akr_webroot))

                # Donwload composer and install ViMbAdmin
                Log.debug(self, "Downloading composer "
                          "https://getcomposer.org/installer | php ")
                try:
                    AKRShellExec.cmd_exec(self, "cd {0}22222/htdocs"
                                         "/vimbadmin; curl"
                                         " -sS https://getcomposer.org/"
                                         "installer |"
                                         " php".format(AKRVariables.akr_webroot))
                    Log.debug(self, "Installating of composer")
                    AKRShellExec.cmd_exec(self, "cd {0}22222/htdocs"
                                         "/vimbadmin && "
                                         "php composer.phar install "
                                         "--prefer-dist"
                                         " --no-dev && rm -f {1}22222/htdocs"
                                         "/vimbadmin/composer.phar"
                                         .format(AKRVariables.akr_webroot,
                                                 AKRVariables.akr_webroot))
                except CommandExecutionError as e:
                    raise SiteError("Failed to setup ViMbAdmin")

                # Configure vimbadmin database
                vm_passwd = ''.join(random.sample(string.ascii_letters, 8))
                Log.debug(self, "Creating vimbadmin database if not exist")
                AKRMysql.execute(self, "create database if not exists"
                                      " vimbadmin")
                Log.debug(self, " grant all privileges on `vimbadmin`.* to"
                                " `vimbadmin`@`{0}` IDENTIFIED BY"
                                " ' '".format(self.app.config.get('mysql',
                                              'grant-host')))
                AKRMysql.execute(self, "grant all privileges on `vimbadmin`.* "
                                " to `vimbadmin`@`{0}` IDENTIFIED BY"
                                " '{1}'".format(self.app.config.get('mysql',
                                                'grant-host'), vm_passwd),
                                errormsg="Cannot grant "
                                "user privileges", log=False)
                vm_salt = (''.join(random.sample(string.ascii_letters +
                                                 string.ascii_letters, 64)))

                # Custom Vimbadmin configuration by Akur8Engine
                data = dict(salt=vm_salt, host=AKRVariables.akr_mysql_host,
                            password=vm_passwd,
                            php_user=AKRVariables.akr_php_user)
                Log.debug(self, 'Writting the ViMbAdmin configuration to '
                          'file {0}22222/htdocs/vimbadmin/application/'
                          'configs/application.ini'
                          .format(AKRVariables.akr_webroot))
                akr_vmb = open('{0}22222/htdocs/vimbadmin/application/'
                              'configs/application.ini'
                              .format(AKRVariables.akr_webroot),
                              encoding='utf-8', mode='w')
                self.app.render((data), 'vimbadmin.mustache',
                                out=akr_vmb)
                akr_vmb.close()

                shutil.copyfile("{0}22222/htdocs/vimbadmin/public/"
                                ".htaccess.dist"
                                .format(AKRVariables.akr_webroot),
                                "{0}22222/htdocs/vimbadmin/public/"
                                ".htaccess".format(AKRVariables.akr_webroot))
                Log.debug(self, "Executing command "
                          "{0}22222/htdocs/vimbadmin/bin"
                          "/doctrine2-cli.php orm:schema-tool:"
                          "create".format(AKRVariables.akr_webroot))
                try:
                    AKRShellExec.cmd_exec(self, "{0}22222/htdocs/vimbadmin"
                                         "/bin/doctrine2-cli.php "
                                         "orm:schema-tool:create"
                                         .format(AKRVariables.akr_webroot))
                except CommandExecutionError as e:
                    raise SiteError("Unable to create ViMbAdmin schema")

                AKRFileUtils.chown(self, '{0}22222'
                                  .format(AKRVariables.akr_webroot),
                                  AKRVariables.akr_php_user,
                                  AKRVariables.akr_php_user,
                                  recursive=True)

                # Copy Dovecot and Postfix templates which are depednet on
                # Vimbadmin

                if not os.path.exists('/etc/postfix/mysql/'):
                    Log.debug(self, "Creating directory "
                              "/etc/postfix/mysql/")
                    os.makedirs('/etc/postfix/mysql/')

                if AKRVariables.akr_mysql_host is "localhost":
                    data = dict(password=vm_passwd, host="127.0.0.1")
                else:
                    data = dict(password=vm_passwd,
                                host=AKRVariables.akr_mysql_host)

                vm_config = open('/etc/postfix/mysql/virtual_alias_maps.cf',
                                 encoding='utf-8', mode='w')
                self.app.render((data), 'virtual_alias_maps.mustache',
                                out=vm_config)
                vm_config.close()

                Log.debug(self, "Writting configuration to  "
                          "/etc/postfix/mysql"
                          "/virtual_domains_maps.cf file")
                vm_config = open('/etc/postfix/mysql/virtual_domains_maps.cf',
                                 encoding='utf-8', mode='w')
                self.app.render((data), 'virtual_domains_maps.mustache',
                                out=vm_config)
                vm_config.close()

                Log.debug(self, "Writting configuration to "
                          "/etc/postfix/mysql"
                          "/virtual_mailbox_maps.cf file")
                vm_config = open('/etc/postfix/mysql/virtual_mailbox_maps.cf',
                                 encoding='utf-8', mode='w')
                self.app.render((data), 'virtual_mailbox_maps.mustache',
                                out=vm_config)
                vm_config.close()

                Log.debug(self, "Writting configration"
                                " to /etc/dovecot/dovecot-sql.conf.ext file ")
                vm_config = open('/etc/dovecot/dovecot-sql.conf.ext',
                                 encoding='utf-8', mode='w')
                self.app.render((data), 'dovecot-sql-conf.mustache',
                                out=vm_config)
                vm_config.close()

                # If Amavis is going to be installed then configure Vimabadmin
                # Amvis settings
                if set(AKRVariables.akr_mailscanner).issubset(set(apt_packages)):
                    vm_config = open('/etc/amavis/conf.d/50-user',
                                     encoding='utf-8', mode='w')
                    self.app.render((data), '50-user.mustache',
                                    out=vm_config)
                    vm_config.close()
                AKRService.restart_service(self, 'dovecot')
                AKRService.reload_service(self, 'nginx')
                AKRService.reload_service(self, 'php5-fpm')
                self.msg = (self.msg + ["Configure ViMbAdmin:\thttps://{0}:"
                            "22222/vimbadmin".format(AKRVariables.akr_fqdn)]
                            + ["Security Salt: {0}".format(vm_salt)])

            if any('/tmp/roundcube.tar.gz' == x[1] for x in packages):
                # Extract RoundCubemail
                Log.debug(self, "Extracting file /tmp/roundcube.tar.gz "
                          "to location /tmp/ ")
                AKRExtract.extract(self, '/tmp/roundcube.tar.gz', '/tmp/')
                if not os.path.exists('{0}roundcubemail'
                                      .format(AKRVariables.akr_webroot)):
                    Log.debug(self, "Creating new directory "
                              " {0}roundcubemail/"
                              .format(AKRVariables.akr_webroot))
                    os.makedirs('{0}roundcubemail/'
                                .format(AKRVariables.akr_webroot))
                shutil.move('/tmp/roundcubemail-{0}/'
                            .format(AKRVariables.akr_roundcube),
                            '{0}roundcubemail/htdocs'
                            .format(AKRVariables.akr_webroot))

                # Install Roundcube depednet pear packages
                AKRShellExec.cmd_exec(self, "pear install Mail_Mime Net_SMTP"
                                     " Mail_mimeDecode Net_IDNA2-beta "
                                     "Auth_SASL Net_Sieve Crypt_GPG")

                # Configure roundcube database
                rc_passwd = ''.join(random.sample(string.ascii_letters, 8))
                Log.debug(self, "Creating Database roundcubemail")
                AKRMysql.execute(self, "create database if not exists "
                                " roundcubemail")
                Log.debug(self, "grant all privileges"
                                " on `roundcubemail`.* to "
                                " `roundcube`@`{0}` IDENTIFIED BY "
                                "' '".format(self.app.config.get(
                                             'mysql', 'grant-host')))
                AKRMysql.execute(self, "grant all privileges"
                                " on `roundcubemail`.* to "
                                " `roundcube`@`{0}` IDENTIFIED BY "
                                "'{1}'".format(self.app.config.get(
                                               'mysql', 'grant-host'),
                                               rc_passwd))
                AKRShellExec.cmd_exec(self, "mysql roundcubemail < {0}"
                                     "roundcubemail/htdocs/SQL/mysql"
                                     ".initial.sql"
                                     .format(AKRVariables.akr_webroot))

                shutil.copyfile("{0}roundcubemail/htdocs/config/"
                                "config.inc.php.sample"
                                .format(AKRVariables.akr_webroot),
                                "{0}roundcubemail/htdocs/config/"
                                "config.inc.php"
                                .format(AKRVariables.akr_webroot))
                AKRShellExec.cmd_exec(self, "sed -i \"s\'mysql://roundcube:"
                                     "pass@localhost/roundcubemail\'mysql://"
                                     "roundcube:{0}@{1}/"
                                     "roundcubemail\'\" {2}roundcubemail"
                                     "/htdocs/config/config."
                                     "inc.php"
                                     .format(rc_passwd,
                                             AKRVariables.akr_mysql_host,
                                             AKRVariables.akr_webroot))

                # Sieve npextionsion configuration in roundcube
                AKRShellExec.cmd_exec(self, "bash -c \"sed -i \\\"s:\$config\["
                                     "\'npextionsions\'\] "
                                     "= array(:\$config\['npextionsions'\] =  "
                                     "array(\\n    \'sieverules\',:\\\" "
                                     "{0}roundcubemail/htdocs/config"
                                     .format(AKRVariables.akr_webroot)
                                     + "/config.inc.php\"")
                AKRShellExec.cmd_exec(self, "echo \"\$config['sieverules_port']"
                                     "=4190;\" >> {0}roundcubemail"
                                     .format(AKRVariables.akr_webroot)
                                     + "/htdocs/config/config.inc.php")

                data = dict(site_name='webmail', www_domain='webmail',
                            static=False,
                            basic=True, ik=False, w3tc=False, ikfc=False,
                            iksc=False, multisite=False, iksubdir=False,
                            webroot=AKRVariables.akr_webroot, akr_db_name='',
                            akr_db_user='', akr_db_pass='', akr_db_host='',
                            rc=True)

                Log.debug(self, 'Writting the nginx configuration for '
                          'RoundCubemail')
                akr_rc = open('/etc/nginx/sites-available/webmail',
                             encoding='utf-8', mode='w')
                self.app.render((data), 'virtualconf.mustache',
                                out=akr_rc)
                akr_rc.close()

                # Create Symbolic link for webmail
                AKRFileUtils.create_symlink(self, ['/etc/nginx/sites-available'
                                                  '/webmail',
                                                  '/etc/nginx/sites-enabled/'
                                                  'webmail'])
                # Create log folder and softlinks
                if not os.path.exists('{0}roundcubemail/logs'
                                      .format(AKRVariables.akr_webroot)):
                    os.makedirs('{0}roundcubemail/logs'
                                .format(AKRVariables.akr_webroot))

                AKRFileUtils.create_symlink(self, ['/var/log/nginx/'
                                                  'webmail.access.log',
                                                  '{0}roundcubemail/'
                                                  'logs/access.log'
                                           .format(AKRVariables.akr_webroot)])

                AKRFileUtils.create_symlink(self, ['/var/log/nginx/'
                                                  'webmail.error.log',
                                                  '{0}roundcubemail/'
                                                  'logs/error.log'
                                           .format(AKRVariables.akr_webroot)])
                # Remove roundcube installer
                AKRService.reload_service(self, 'nginx')
                AKRFileUtils.remove(self, ["{0}roundcubemail/htdocs/installer"
                                   .format(AKRVariables.akr_webroot)])
                AKRFileUtils.chown(self, '{0}roundcubemail'
                                  .format(AKRVariables.akr_webroot),
                                  AKRVariables.akr_php_user,
                                  AKRVariables.akr_php_user,
                                  recursive=True)

            if any('/tmp/pra.tar.gz' == x[1]
                    for x in packages):
                Log.debug(self, 'Extracting file /tmp/pra.tar.gz to '
                          'loaction /tmp/')
                AKRExtract.extract(self, '/tmp/pra.tar.gz', '/tmp/')
                if not os.path.exists('{0}22222/htdocs/cache/redis'
                                      .format(AKRVariables.akr_webroot)):
                    Log.debug(self, "Creating new directory "
                              "{0}22222/htdocs/cache/redis"
                              .format(AKRVariables.akr_webroot))
                    os.makedirs('{0}22222/htdocs/cache/redis'
                                .format(AKRVariables.akr_webroot))
                shutil.move('/tmp/phpRedisAdmin-master/',
                            '{0}22222/htdocs/cache/redis/phpRedisAdmin'
                            .format(AKRVariables.akr_webroot))

                Log.debug(self, 'Extracting file /tmp/predis.tar.gz to '
                          'loaction /tmp/')
                AKRExtract.extract(self, '/tmp/predis.tar.gz', '/tmp/')
                shutil.move('/tmp/predis-1.0.1/',
                            '{0}22222/htdocs/cache/redis/phpRedisAdmin/vendor'
                            .format(AKRVariables.akr_webroot))

                Log.debug(self, 'Setting Privileges of webroot permission to  '
                          '{0}22222/htdocs/cache/ file '
                          .format(AKRVariables.akr_webroot))
                AKRFileUtils.chown(self, '{0}22222'
                                  .format(AKRVariables.akr_webroot),
                                  AKRVariables.akr_php_user,
                                  AKRVariables.akr_php_user,
                                  recursive=True)

    @expose(help="Install packages")
    def install(self, packages=[], apt_packages=[], disp_msg=True):
        """Start installation of packages"""
        self.msg = []
        try:
            # Default action for stack installation
            if ((not self.app.pargs.web) and (not self.app.pargs.admin) and
               (not self.app.pargs.mail) and (not self.app.pargs.nginx) and
               (not self.app.pargs.php) and (not self.app.pargs.mysql) and
               (not self.app.pargs.postfix) and (not self.app.pargs.ikcli) and
               (not self.app.pargs.phpmyadmin) and (not self.app.pargs.hhvm)
               and (not self.app.pargs.pagespeed) and
               (not self.app.pargs.adminer) and (not self.app.pargs.utils) and
               (not self.app.pargs.mailscanner) and (not self.app.pargs.all)
               and (not self.app.pargs.redis) and (not self.app.pargs.nginxmainline) and
               (not self.app.pargs.phpredisadmin)):
                self.app.pargs.web = True
                self.app.pargs.admin = True

            if self.app.pargs.all:
                self.app.pargs.web = True
                self.app.pargs.admin = True
                self.app.pargs.mail = True

            if self.app.pargs.web:
                self.app.pargs.nginx = True
                self.app.pargs.php = True
                self.app.pargs.mysql = True
                self.app.pargs.ikcli = True
                self.app.pargs.postfix = True

            if self.app.pargs.admin:
                self.app.pargs.nginx = True
                self.app.pargs.php = True
                self.app.pargs.mysql = True
                self.app.pargs.adminer = True
                self.app.pargs.phpmyadmin = True
                self.app.pargs.utils = True

            if self.app.pargs.mail:
                self.app.pargs.nginx = True
                self.app.pargs.php = True
                self.app.pargs.mysql = True
                self.app.pargs.postfix = True

                if not AKRAptGet.is_installed(self, 'dovecot-core'):
                    check_fqdn(self,
                               os.popen("hostname -f | tr -d '\n'").read())
                    Log.debug(self, "Setting apt_packages variable for mail")
                    apt_packages = apt_packages + AKRVariables.akr_mail
                    packages = packages + [["https://github.com/opensolutions/"
                                            "ViMbAdmin/archive/{0}.tar.gz"
                                            .format(AKRVariables.akr_vimbadmin),
                                            "/tmp/vimbadmin.tar.gz",
                                            "ViMbAdmin"],
                                           ["https://github.com/roundcube/"
                                            "roundcubemail/releases/download/"
                                            "{0}/roundcubemail-{0}.tar.gz"
                                            .format(AKRVariables.akr_roundcube),
                                            "/tmp/roundcube.tar.gz",
                                            "Roundcube"]]

                    if AKRVariables.akr_ram > 1024:
                        self.app.pargs.mailscanner = True
                    else:
                        Log.info(self, "System RAM is less than 1GB\nMail "
                                 "scanner packages are not going to install"
                                 " automatically")
                else:
                    Log.info(self, "Mail server is already installed")

            if self.app.pargs.pagespeed:
                if not (AKRAptGet.is_installed(self, 'nginx-custom') or AKRAptGet.is_installed(self, 'nginx-mainline')):
                    self.app.pargs.nginx = True
                else:
                    Log.info(self, "Nginx already installed")

            if self.app.pargs.redis:
                if not AKRAptGet.is_installed(self, 'redis-server'):
                    apt_packages = apt_packages + AKRVariables.akr_redis
                    self.app.pargs.php = True
                else:
                    Log.info(self, "Redis already installed")

            if self.app.pargs.nginx:
                Log.debug(self, "Setting apt_packages variable for Nginx")

                if not (AKRAptGet.is_installed(self, 'nginx-custom') or  AKRAptGet.is_installed(self, 'nginx-mainline')):
                    if not AKRAptGet.is_installed(self, 'nginx-plus'):
                        apt_packages = apt_packages + AKRVariables.akr_nginx
                    else:
                        Log.info(self, "NGINX PLUS Detected ...")
                        apt = ["nginx-plus"] + AKRVariables.akr_nginx
                        #apt_packages = apt_packages + AKRVariables.akr_nginx
                        self.post_pref(apt, packages)
                else:
                    Log.debug(self, "Nginx already installed")
                    if AKRAptGet.is_installed(self, 'nginx-mainline'):
                        Log.warn(self,'Nginx Mainline already found on your system.\nDo you want to remove Nginx mainline '
                                          'and install Nginx Stable.,\nAny answer other than "yes" will be stop this '
                                          'operation.')
                        akr_prompt = input("Type \"YES\" or \"yes\" to continue [n]: ")
                        if akr_prompt == 'YES' or akr_prompt == 'yes':
                            AKRService.stop_service(self, 'nginx')
                            Log.debug(self, "Removing apt_packages variable of NGINX")
                            Log.info(self, "Removing packages, please wait...")
                            AKRAptGet.remove(self, AKRVariables.akr_nginx_dev)
                            AKRAptGet.auto_remove(self)
                            Log.info(self, "Removing repository for NGINX MAINLINE,")
                            AKRRepo.remove(self, repo_url=AKRVariables.akr_nginx_dev_repo)
                            Log.info(self, "Successfully removed packages")
                            Log.info(self,"Initializing NGINX Mainline Packages... Please Wait")
                            if AKRAptGet.download_only(self,AKRVariables.akr_nginx,AKRVariables.akr_nginx_repo,AKRVariables.akr_nginx_key):
                                apt_packages = apt_packages + AKRVariables.akr_nginx
                            else:
                                #revert the changes
                                AKRRepo.add(self, repo_url=AKRVariables.akr_nginx_dev_repo)
                                Log.error(self,"Error installing NGINX Stable\nReverting back to NGINX Mainline ..",False)
                                apt_packages = apt_packages + AKRVariables.akr_nginx_dev
                    else:
                        Log.info(self, "NGINX Stable already installed")

            if self.app.pargs.nginxmainline:
                if AKRVariables.akr_nginx_dev_repo == None:
                    Log.error(self, "NGINX Mainline Version is not supported in your platform")

                Log.debug(self, "Setting apt_packages variable for Nginx")

                if not (AKRAptGet.is_installed(self, 'nginx-custom') or  AKRAptGet.is_installed(self, 'nginx-mainline')):
                    if not AKRAptGet.is_installed(self, 'nginx-plus'):
                        apt_packages = apt_packages + AKRVariables.akr_nginx_dev
                    else:
                        Log.info(self, "NGINX PLUS Detected ...")
                        apt = ["nginx-plus"] + AKRVariables.akr_nginx
                        #apt_packages = apt_packages + AKRVariables.akr_nginx
                        self.post_pref(apt, packages)
                else:
                    Log.debug(self, "Nginx already installed")
                    if AKRAptGet.is_installed(self, 'nginx-custom'):
                        Log.warn(self,'Nginx Stable already found on your system.\nDo you want to remove Nginx stable '
                                          'and install Nginx Mainline.,\nAny answer other than "yes" will be stop this '
                                          'operation.')
                        akr_prompt = input("Type \"YES\" or \"yes\" to continue [n]: ")
                        if akr_prompt == 'YES' or akr_prompt == 'yes':
                            Log.info(self,"Initializing... Please Wait")
                            if AKRAptGet.download_only(self,AKRVariables.akr_nginx_dev,AKRVariables.akr_nginx_dev_repo,AKRVariables.akr_nginx_key):
                                AKRService.stop_service(self, 'nginx')
                                Log.debug(self, "Removing apt_packages variable of Nginx")
                                Log.info(self, "Removing packages, please wait...")
                                AKRAptGet.remove(self, AKRVariables.akr_nginx)
                                AKRAptGet.auto_remove(self)
                                Log.info(self, "Successfully removed packages")
                                apt_packages = apt_packages + AKRVariables.akr_nginx_dev
                            else:
                                Log.error(self,"Skipped installing NGINX Mainline",False)
                    else:
                        Log.info(self, "NGINX Mainline already installed")

            if self.app.pargs.php:
                Log.debug(self, "Setting apt_packages variable for PHP")
                if not AKRAptGet.is_installed(self, 'php5-fpm'):
                    apt_packages = apt_packages + AKRVariables.akr_php
                else:
                    Log.debug(self, "PHP already installed")
                    Log.info(self, "PHP already installed")

            if self.app.pargs.hhvm:
                Log.debug(self, "Setting apt packages variable for HHVM")
                if platform.architecture()[0] is '32bit':
                    Log.error(self, "HHVM is not supported by 32bit system")
                if not AKRAptGet.is_installed(self, 'hhvm'):
                    apt_packages = apt_packages + AKRVariables.akr_hhvm
                else:
                    Log.debug(self, "HHVM already installed")
                    Log.info(self, "HHVM already installed")

            if self.app.pargs.mysql:
                Log.debug(self, "Setting apt_packages variable for MySQL")
                if not AKRShellExec.cmd_exec(self, "mysqladmin ping"):
                    apt_packages = apt_packages + AKRVariables.akr_mysql
                    packages = packages + [["https://raw."
                                            "githubusercontent.com/"
                                            "major/MySQLTuner-perl"
                                            "/master/mysqltuner.pl",
                                            "/usr/bin/mysqltuner",
                                            "MySQLTuner"]]

                else:
                    Log.debug(self, "MySQL connection is already alive")
                    Log.info(self, "MySQL connection is already alive")
            if self.app.pargs.postfix:
                Log.debug(self, "Setting apt_packages variable for Postfix")
                if not AKRAptGet.is_installed(self, 'postfix'):
                    apt_packages = apt_packages + AKRVariables.akr_postfix
                else:
                    Log.debug(self, "Postfix is already installed")
                    Log.info(self, "Postfix is already installed")
            if self.app.pargs.ikcli:
                Log.debug(self, "Setting packages variable for IK-CLI")
                if not AKRShellExec.cmd_exec(self, "which ik"):
                    packages = packages + [["https://github.com/ik-cli/ik-cli/"
                                            "releases/download/v{0}/"
                                            "ik-cli-{0}.phar"
                                            "".format(AKRVariables.akr_ik_cli),
                                            "/usr/bin/ik",
                                            "IK-CLI"]]
                else:
                    Log.debug(self, "IK-CLI is already installed")
                    Log.info(self, "IK-CLI is already installed")
            if self.app.pargs.phpmyadmin:
                Log.debug(self, "Setting packages varible for phpMyAdmin ")
                packages = packages + [["https://github.com/phpmyadmin/"
                                        "phpmyadmin/archive/STABLE.tar.gz",
                                        "/tmp/pma.tar.gz", "phpMyAdmin"]]

            if self.app.pargs.phpredisadmin:
                Log.debug(self, "Setting packages varible for phpRedisAdmin")
                packages = packages + [["https://github.com/ErikDubbelboer/"
                                        "phpRedisAdmin/archive/master.tar.gz",
                                        "/tmp/pra.tar.gz","phpRedisAdmin"],
                                       ["https://github.com/nrk/predis/"
                                        "archive/v1.0.1.tar.gz",
                                        "/tmp/predis.tar.gz", "Predis"]]

            if self.app.pargs.adminer:
                Log.debug(self, "Setting packages variable for Adminer ")
                packages = packages + [["http://downloads.sourceforge.net/"
                                        "adminer/adminer-{0}.php"
                                        "".format(AKRVariables.akr_adminer),
                                        "{0}22222/"
                                        "htdocs/db/adminer/index.php"
                                        .format(AKRVariables.akr_webroot),
                                        "Adminer"]]

            if self.app.pargs.mailscanner:
                if not AKRAptGet.is_installed(self, 'amavisd-new'):
                    if (AKRAptGet.is_installed(self, 'dovecot-core') or
                       self.app.pargs.mail):
                        apt_packages = (apt_packages +
                                        AKRVariables.akr_mailscanner)
                    else:
                        Log.error(self, "Failed to find installed Dovecot")
                else:
                    Log.error(self, "Mail scanner already installed")

            if self.app.pargs.utils:
                Log.debug(self, "Setting packages variable for utils")
                packages = packages + [["http://phpmemcacheadmin.googlecode"
                                        ".com/files/phpMemcachedAdmin-1.2.2"
                                        "-r262.tar.gz", '/tmp/memcache.tar.gz',
                                        'phpMemcachedAdmin'],
                                       ["https://raw.githubusercontent.com"
                                        "/rtCamp/akradmin/master/cache/nginx/"
                                        "clean.php",
                                        "{0}22222/htdocs/cache/"
                                        "nginx/clean.php"
                                        .format(AKRVariables.akr_webroot),
                                        "clean.php"],
                                       ["https://raw.github.com/rlerdorf/"
                                        "opcache-status/master/opcache.php",
                                        "{0}22222/htdocs/cache/"
                                        "opcache/opcache.php"
                                        .format(AKRVariables.akr_webroot),
                                        "opcache.php"],
                                       ["https://raw.github.com/amnuts/"
                                        "opcache-gui/master/index.php",
                                        "{0}22222/htdocs/"
                                        "cache/opcache/opgui.php"
                                        .format(AKRVariables.akr_webroot),
                                        "Opgui"],
                                       ["https://gist.github.com/ck-on/4959032"
                                        "/raw/0b871b345fd6cfcd6d2be030c1f33d1"
                                        "ad6a475cb/ocp.php",
                                        "{0}22222/htdocs/cache/"
                                        "opcache/ocp.php"
                                        .format(AKRVariables.akr_webroot),
                                        "OCP.php"],
                                       ["https://github.com/jokkedk/webgrind/"
                                        "archive/master.tar.gz",
                                        '/tmp/webgrind.tar.gz', 'Webgrind'],
                                       ["http://bazaar.launchpad.net/~"
                                        "percona-toolkit-dev/percona-toolkit/"
                                        "2.1/download/head:/ptquerydigest-"
                                        "20110624220137-or26tn4"
                                        "expb9ul2a-16/pt-query-digest",
                                        "/usr/bin/pt-query-advisor",
                                        "pt-query-advisor"],
                                       ["https://github.com/box/Anemometer/"
                                        "archive/master.tar.gz",
                                        '/tmp/anemometer.tar.gz', 'Anemometer']
                                       ]
        except Exception as e:
            pass

        if len(apt_packages) or len(packages):
            Log.debug(self, "Calling pre_pref")
            self.pre_pref(apt_packages)
            if len(apt_packages):
                AKRSwap.add(self)
                Log.info(self, "Updating apt-cache, please wait...")
                AKRAptGet.update(self)
                Log.info(self, "Installing packages, please wait...")
                AKRAptGet.install(self, apt_packages)
            if len(packages):
                Log.debug(self, "Downloading following: {0}".format(packages))
                AKRDownload.download(self, packages)
            Log.debug(self, "Calling post_pref")
            self.post_pref(apt_packages, packages)
            if 'redis-server' in apt_packages:
                # set redis.conf parameter
                # set maxmemory 10% for ram below 512MB and 20% for others
                # set maxmemory-policy volatile-lru
                if os.path.isfile("/etc/redis/redis.conf"):
                    if AKRVariables.akr_ram < 512:
                        Log.debug(self, "Setting maxmemory variable to {0} in redis.conf"
                                            .format(int(AKRVariables.akr_ram*1024*1024*0.1)))
                        AKRShellExec.cmd_exec(self, "sed -i 's/# maxmemory <bytes>/maxmemory {0}/' /etc/redis/redis.conf"
                                             .format(int(AKRVariables.akr_ram*1024*1024*0.1)))
                        Log.debug(self, "Setting maxmemory-policy variable to volatile-lru in redis.conf")
                        AKRShellExec.cmd_exec(self, "sed -i 's/# maxmemory-policy.*/maxmemory-policy volatile-lru/' "
                                                   "/etc/redis/redis.conf")
                        AKRService.restart_service(self, 'redis-server')
                    else:
                        Log.debug(self, "Setting maxmemory variable to {0} in redis.conf"
                                            .format(int(AKRVariables.akr_ram*1024*1024*0.2)))
                        AKRShellExec.cmd_exec(self, "sed -i 's/# maxmemory <bytes>/maxmemory {0}/' /etc/redis/redis.conf"
                                             .format(int(AKRVariables.akr_ram*1024*1024*0.2)))
                        Log.debug(self, "Setting maxmemory-policy variable to volatile-lru in redis.conf")
                        AKRShellExec.cmd_exec(self, "sed -i 's/# maxmemory-policy.*/maxmemory-policy volatile-lru/' "
                                                   "/etc/redis/redis.conf")
                        AKRService.restart_service(self, 'redis-server')
            if disp_msg:
                if len(self.msg):
                    for msg in self.msg:
                        Log.info(self, Log.ENDC + msg)
                Log.info(self, "Successfully installed packages")
            else:
                return self.msg

    @expose(help="Remove packages")
    def remove(self):
        """Start removal of packages"""
        apt_packages = []
        packages = []

        # Default action for stack remove
        if ((not self.app.pargs.web) and (not self.app.pargs.admin) and
           (not self.app.pargs.mail) and (not self.app.pargs.nginx) and
           (not self.app.pargs.php) and (not self.app.pargs.mysql) and
           (not self.app.pargs.postfix) and (not self.app.pargs.ikcli) and
           (not self.app.pargs.phpmyadmin) and (not self.app.pargs.hhvm) and
           (not self.app.pargs.adminer) and (not self.app.pargs.utils) and
           (not self.app.pargs.mailscanner) and (not self.app.pargs.all) and
           (not self.app.pargs.pagespeed) and (not self.app.pargs.redis) and
           (not self.app.pargs.phpredisadmin) and (not self.app.pargs.nginxmainline)):
            self.app.pargs.web = True
            self.app.pargs.admin = True

        if self.app.pargs.all:
            self.app.pargs.web = True
            self.app.pargs.admin = True
            self.app.pargs.mail = True

        if self.app.pargs.web:
            self.app.pargs.nginx = True
            self.app.pargs.php = True
            self.app.pargs.mysql = True
            self.app.pargs.ikcli = True
            self.app.pargs.postfix = True

        if self.app.pargs.admin:
            self.app.pargs.adminer = True
            self.app.pargs.phpmyadmin = True
            self.app.pargs.utils = True

        if self.app.pargs.mail:
            Log.debug(self, "Removing mail server packages")
            apt_packages = apt_packages + AKRVariables.akr_mail
            apt_packages = apt_packages + AKRVariables.akr_mailscanner
            packages = packages + ["{0}22222/htdocs/vimbadmin"
                                   .format(AKRVariables.akr_webroot),
                                   "{0}roundcubemail"
                                   .format(AKRVariables.akr_webroot)]
            if AKRShellExec.cmd_exec(self, "mysqladmin ping"):
                AKRMysql.execute(self, "drop database IF EXISTS vimbadmin")
                AKRMysql.execute(self, "drop database IF EXISTS roundcubemail")

        if self.app.pargs.mailscanner:
            apt_packages = (apt_packages + AKRVariables.akr_mailscanner)

        if self.app.pargs.pagespeed:
            Log.debug(self, "Removing packages varible of Pagespeed")
            packages = packages + ['/etc/nginx/conf.d/pagespeed.conf']

        if self.app.pargs.nginx:
            if AKRAptGet.is_installed(self, 'nginx-custom'):
                Log.debug(self, "Removing apt_packages variable of Nginx")
                apt_packages = apt_packages + AKRVariables.akr_nginx
            else:
                Log.error(self,"Cannot Remove! Nginx Stable version not found.")
        if self.app.pargs.nginxmainline:
            if AKRAptGet.is_installed(self, 'nginx-mainline'):
                Log.debug(self, "Removing apt_packages variable of Nginx MAINLINE")
                apt_packages = apt_packages + AKRVariables.akr_nginx_dev
            else:
                Log.error(self,"Cannot Remove! Nginx Mainline version not found.")
        if self.app.pargs.php:
            Log.debug(self, "Removing apt_packages variable of PHP")
            apt_packages = apt_packages + AKRVariables.akr_php

        if self.app.pargs.hhvm:
            if AKRAptGet.is_installed(self, 'hhvm'):
                Log.debug(self, "Removing apt_packages varible of HHVM")
                apt_packages = apt_packages + AKRVariables.akr_hhvm
        if self.app.pargs.redis:
            Log.debug(self, "Remove apt_packages variable of Redis")
            apt_packages = apt_packages + AKRVariables.akr_redis
        if self.app.pargs.mysql:
            Log.debug(self, "Removing apt_packages variable of MySQL")
            apt_packages = apt_packages + AKRVariables.akr_mysql
            packages = packages + ['/usr/bin/mysqltuner']
        if self.app.pargs.postfix:
            Log.debug(self, "Removing apt_packages variable of Postfix")
            apt_packages = apt_packages + AKRVariables.akr_postfix
        if self.app.pargs.ikcli:
            Log.debug(self, "Removing package variable of IKCLI ")
            if os.path.isfile('/usr/bin/ik'):
                packages = packages + ['/usr/bin/ik']
            else:
                Log.warn(self, "IK-CLI is not installed with Akur8Engine")
        if self.app.pargs.phpmyadmin:
            Log.debug(self, "Removing package variable of phpMyAdmin ")
            packages = packages + ['{0}22222/htdocs/db/pma'
                                   .format(AKRVariables.akr_webroot)]
        if self.app.pargs.phpredisadmin:
            Log.debug(self, "Removing package variable of phpRedisAdmin ")
            packages = packages + ['{0}22222/htdocs/cache/redis/phpRedisAdmin'
                                   .format(AKRVariables.akr_webroot)]
        if self.app.pargs.adminer:
            Log.debug(self, "Removing package variable of Adminer ")
            packages = packages + ['{0}22222/htdocs/db/adminer'
                                   .format(AKRVariables.akr_webroot)]
        if self.app.pargs.utils:
            Log.debug(self, "Removing package variable of utils ")
            packages = packages + ['{0}22222/htdocs/php/webgrind/'
                                   .format(AKRVariables.akr_webroot),
                                   '{0}22222/htdocs/cache/opcache'
                                   .format(AKRVariables.akr_webroot),
                                   '{0}22222/htdocs/cache/nginx/'
                                   'clean.php'.format(AKRVariables.akr_webroot),
                                   '{0}22222/htdocs/cache/memcache'
                                   .format(AKRVariables.akr_webroot),
                                   '/usr/bin/pt-query-advisor',
                                   '{0}22222/htdocs/db/anemometer'
                                   .format(AKRVariables.akr_webroot)]

        if len(packages) or len(apt_packages):
            akr_prompt = input('Are you sure you to want to'
                              ' remove from server.'
                              '\nPackage configuration will remain'
                              ' on server after this operation.\n'
                              'Any answer other than '
                              '"yes" will be stop this'
                              ' operation :  ')

            if akr_prompt == 'YES' or akr_prompt == 'yes':

                if (set(["nginx-mainline"]).issubset(set(apt_packages)) or
                        set(["nginx-custom"]).issubset(set(apt_packages))) :
                    AKRService.stop_service(self, 'nginx')

                if len(packages):
                    AKRFileUtils.remove(self, packages)
                    AKRAptGet.auto_remove(self)

                if len(apt_packages):
                    Log.debug(self, "Removing apt_packages")
                    Log.info(self, "Removing packages, please wait...")
                    AKRAptGet.remove(self, apt_packages)
                    AKRAptGet.auto_remove(self)

                if set(["nginx-mainline"]).issubset(set(apt_packages)):
                    Log.info(self, "Removing repository for NGINX MAINLINE,")
                    AKRRepo.remove(self, repo_url=AKRVariables.akr_nginx_dev_repo)


                Log.info(self, "Successfully removed packages")

    @expose(help="Purge packages")
    def purge(self):
        """Start purging of packages"""
        apt_packages = []
        packages = []

        # Default action for stack purge
        if ((not self.app.pargs.web) and (not self.app.pargs.admin) and
           (not self.app.pargs.mail) and (not self.app.pargs.nginx) and
           (not self.app.pargs.php) and (not self.app.pargs.mysql) and
           (not self.app.pargs.postfix) and (not self.app.pargs.ikcli) and
           (not self.app.pargs.phpmyadmin) and (not self.app.pargs.hhvm) and
           (not self.app.pargs.adminer) and (not self.app.pargs.utils) and
           (not self.app.pargs.mailscanner) and (not self.app.pargs.all) and
           (not self.app.pargs.pagespeed) and (not self.app.pargs.redis) and
           (not self.app.pargs.phpredisadmin) and (not self.app.pargs.nginxmainline)):
            self.app.pargs.web = True
            self.app.pargs.admin = True

        if self.app.pargs.all:
            self.app.pargs.web = True
            self.app.pargs.admin = True
            self.app.pargs.mail = True

        if self.app.pargs.web:
            self.app.pargs.nginx = True
            self.app.pargs.php = True
            self.app.pargs.mysql = True
            self.app.pargs.ikcli = True
            self.app.pargs.postfix = True

        if self.app.pargs.admin:
            self.app.pargs.adminer = True
            self.app.pargs.phpmyadmin = True
            self.app.pargs.utils = True

        if self.app.pargs.mail:
            Log.debug(self, "Removing mail server packages")
            apt_packages = apt_packages + AKRVariables.akr_mail
            apt_packages = apt_packages + AKRVariables.akr_mailscanner
            packages = packages + ["{0}22222/htdocs/vimbadmin"
                                   .format(AKRVariables.akr_webroot),
                                   "{0}roundcubemail"
                                   .format(AKRVariables.akr_webroot)]
            if AKRShellExec.cmd_exec(self, "mysqladmin ping"):
                AKRMysql.execute(self, "drop database IF EXISTS vimbadmin")
                AKRMysql.execute(self, "drop database IF EXISTS roundcubemail")

        if self.app.pargs.mailscanner:
            apt_packages = (apt_packages + AKRVariables.akr_mailscanner)

        if self.app.pargs.pagespeed:
            Log.debug(self, "Purge packages varible of Pagespeed")
            packages = packages + ['/etc/nginx/conf.d/pagespeed.conf']

        if self.app.pargs.nginx:
            if AKRAptGet.is_installed(self, 'nginx-custom'):
                Log.debug(self, "Purge apt_packages variable of Nginx")
                apt_packages = apt_packages + AKRVariables.akr_nginx
            else:
                Log.error(self,"Cannot Purge! Nginx Stable version not found.")
        if self.app.pargs.nginxmainline:
            if AKRAptGet.is_installed(self, 'nginx-mainline'):
                Log.debug(self, "Purge apt_packages variable of Nginx Mainline")
                apt_packages = apt_packages + AKRVariables.akr_nginx_dev
            else:
                Log.error(self,"Cannot Purge! Nginx Mainline version not found.")
        if self.app.pargs.php:
            Log.debug(self, "Purge apt_packages variable PHP")
            apt_packages = apt_packages + AKRVariables.akr_php
        if self.app.pargs.hhvm:
            if AKRAptGet.is_installed(self, 'hhvm'):
                Log.debug(self, "Purge apt_packages varible of HHVM")
                apt_packages = apt_packages + AKRVariables.akr_hhvm
        if self.app.pargs.redis:
            Log.debug(self, "Purge apt_packages variable of Redis")
            apt_packages = apt_packages + AKRVariables.akr_redis
        if self.app.pargs.mysql:
            Log.debug(self, "Purge apt_packages variable MySQL")
            apt_packages = apt_packages + AKRVariables.akr_mysql
            packages = packages + ['/usr/bin/mysqltuner']
        if self.app.pargs.postfix:
            Log.debug(self, "Purge apt_packages variable PostFix")
            apt_packages = apt_packages + AKRVariables.akr_postfix
        if self.app.pargs.ikcli:
            Log.debug(self, "Purge package variable IKCLI")
            if os.path.isfile('/usr/bin/ik'):
                packages = packages + ['/usr/bin/ik']
            else:
                Log.warn(self, "IK-CLI is not installed with Akur8Engine")
        if self.app.pargs.phpmyadmin:
            packages = packages + ['{0}22222/htdocs/db/pma'.
                                   format(AKRVariables.akr_webroot)]
            Log.debug(self, "Purge package variable phpMyAdmin")
        if self.app.pargs.phpredisadmin:
            Log.debug(self, "Removing package variable of phpRedisAdmin ")
            packages = packages + ['{0}22222/htdocs/cache/redis/phpRedisAdmin'
                                   .format(AKRVariables.akr_webroot)]
        if self.app.pargs.adminer:
            Log.debug(self, "Purge  package variable Adminer")
            packages = packages + ['{0}22222/htdocs/db/adminer'
                                   .format(AKRVariables.akr_webroot)]
        if self.app.pargs.utils:
            Log.debug(self, "Purge package variable utils")
            packages = packages + ['{0}22222/htdocs/php/webgrind/'
                                   .format(AKRVariables.akr_webroot),
                                   '{0}22222/htdocs/cache/opcache'
                                   .format(AKRVariables.akr_webroot),
                                   '{0}22222/htdocs/cache/nginx/'
                                   'clean.php'.format(AKRVariables.akr_webroot),
                                   '{0}22222/htdocs/cache/memcache'
                                   .format(AKRVariables.akr_webroot),
                                   '/usr/bin/pt-query-advisor',
                                   '{0}22222/htdocs/db/anemometer'
                                   .format(AKRVariables.akr_webroot)
                                   ]

        if len(packages) or len(apt_packages):
            akr_prompt = input('Are you sure you to want to purge '
                              'from server '
                              'along with their configuration'
                              ' packages,\nAny answer other than '
                              '"yes" will be stop this '
                              'operation :')

            if akr_prompt == 'YES' or akr_prompt == 'yes':

                if (set(["nginx-mainline"]).issubset(set(apt_packages)) or
                        set(["nginx-custom"]).issubset(set(apt_packages))) :
                    AKRService.stop_service(self, 'nginx')

                if len(apt_packages):
                    Log.info(self, "Purging packages, please wait...")
                    AKRAptGet.remove(self, apt_packages, purge=True)
                    AKRAptGet.auto_remove(self)

                if len(packages):
                    AKRFileUtils.remove(self, packages)
                    AKRAptGet.auto_remove(self)

                if set(["nginx-mainline"]).issubset(set(apt_packages)):
                    Log.info(self, "Removing repository for NGINX MAINLINE,")
                    AKRRepo.remove(self, repo_url=AKRVariables.akr_nginx_dev_repo)


                Log.info(self, "Successfully purged packages")

def load(app):
    # register the npextionsion class.. this only happens if the npextionsion is enabled
    handler.register(AKRStackController)
    handler.register(AKRStackStatusController)
    handler.register(AKRStackMigrateController)
    handler.register(AKRStackUpgradeController)

    # register a hook (function) to run after arguments are parsed.
    hook.register('post_argument_parsing', akr_stack_hook)
