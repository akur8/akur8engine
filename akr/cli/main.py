"""Akur8Engine main application entry point."""
import sys
import os

# this has to happen after you import sys, but before you import anything
# from Cement "source: https://github.com/datafolklabs/cement/issues/290"
if '--debug' in sys.argv:
    sys.argv.remove('--debug')
    TOGGLE_DEBUG = True
else:
    TOGGLE_DEBUG = False

from cement.core import foundation
from cement.utils.misc import init_defaults
from cement.core.exc import FrameworkError, CaughtSignal
from cement.ext.ext_argparse import ArgParseArgumentHandler
from akr.core import exc
from akr.cli.ext.akr_outputhandler import AKROutputHandler

# Application default.  Should update config/akr.conf to reflect any
# changes, or additions here.
defaults = init_defaults('akr')

# All internal/external npextionsion configurations are loaded from here
defaults['akr']['npextionsion_config_dir'] = '/etc/akr/npextionsions.d'

# External npextionsions (generally, do not ship with application code)
defaults['akr']['npextionsion_dir'] = '/var/lib/akr/npextionsions'

# External templates (generally, do not ship with application code)
defaults['akr']['template_dir'] = '/var/lib/akr/templates'


class AKRArgHandler(ArgParseArgumentHandler):
    class Meta:
        label = 'akr_args_handler'

    def error(self, message):
        super(AKRArgHandler, self).error("unknown args")


class AKRApp(foundation.CementApp):
    class Meta:
        label = 'akr'

        config_defaults = defaults

        # All built-in application bootstrapping (always run)
        bootstrap = 'akr.cli.bootstrap'

        # Optional npextionsion bootstrapping (only run if npextionsion is enabled)
        npextionsion_bootstrap = 'akr.cli.npextionsions'

        # Internal templates (ship with application code)
        template_module = 'akr.cli.templates'

        # Internal npextionsions (ship with application code)
        npextionsion_bootstrap = 'akr.cli.npextionsions'

        extensions = ['mustache']

        # default output handler
        output_handler = AKROutputHandler

        arg_handler = AKRArgHandler

        debug = TOGGLE_DEBUG


class AKRTestApp(AKRApp):
    """A test app that is better suited for testing."""
    class Meta:
        argv = []
        config_files = []


# Define the applicaiton object outside of main, as some libraries might wish
# to import it as a global (rather than passing it into another class/func)
app = AKRApp()


def main():
    try:
        global sys
        # Default our exit status to 0 (non-error)
        code = 0

        # if not root...kick out
        if not os.geteuid() == 0:
            print("\nOnly root or sudo user can run this Akur8Engine\n")
            app.close(1)

        # Setup the application
        app.setup()

        # Dump all arguments into akr log
        app.log.debug(sys.argv)

        # Run the application
        app.run()
    except exc.AKRError as e:
        # Catch our application errors and exit 1 (error)
        code = 1
        print(e)
    except FrameworkError as e:
        # Catch framework errors and exit 1 (error)
        code = 1
        print(e)
    except CaughtSignal as e:
        # Default Cement signals are SIGINT and SIGTERM, exit 0 (non-error)
        code = 0
        print(e)
    except Exception as e:
        code = 1
        print(e)
    finally:
        # Print an exception (if it occurred) and --debug was passed
        if app.debug:
            import sys
            import traceback

            exc_type, exc_value, exc_traceback = sys.exc_info()
            if exc_traceback is not None:
                traceback.print_exc()

        # # Close the application
    app.close(code)


def get_test_app(**kw):
    app = AKRApp(**kw)
    return app

if __name__ == '__main__':
    main()
