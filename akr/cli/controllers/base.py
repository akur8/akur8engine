"""Akur8Engine base controller."""

from cement.core.controller import CementBaseController, expose
from akr.core.variables import AKRVariables
VERSION = AKRVariables.akr_version

BANNER = """
Akur8Engine v%s
Copyright (c) 2015 rtCamp Solutions Pvt. Ltd. 
""" % VERSION


class AKRBaseController(CementBaseController):
    class Meta:
        label = 'base'
        description = ("Akur8Engine is the commandline tool to manage your"
                       " websites based on NanoPublisher and Nginx with easy to"
                       " use commands")
        arguments = [
            (['-v', '--version'], dict(action='version', version=BANNER)),
            ]

    @expose(hide=True)
    def default(self):
        self.app.args.print_help()
