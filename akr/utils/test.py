"""Testing utilities for Akur8Engine."""
from akr.cli.main import AKRTestApp
from cement.utils.test import *


class AKRTestCase(CementTestCase):
    app_class = AKRTestApp

    def setUp(self):
        """Override setup actions (for every test)."""
        super(AKRTestCase, self).setUp()

    def tearDown(self):
        """Override teardown actions (for every test)."""
        super(AKRTestCase, self).tearDown()
