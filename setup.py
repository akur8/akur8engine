
from setuptools import setup, find_packages
import sys
import os
import glob
import configparser
import re
import shutil

conf = []
templates = []

long_description = '''Akur8Engine is the commandline tool to manage your
                      Websites based on NanoPublisher and Nginx with easy to use
                      commands'''

for name in glob.glob('config/npextionsions.d/*.conf'):
    conf.insert(1, name)

for name in glob.glob('akr/cli/templates/*.mustache'):
    templates.insert(1, name)

if not os.path.exists('/var/log/akr/'):
    os.makedirs('/var/log/akr/')

if not os.path.exists('/var/lib/akr/'):
    os.makedirs('/var/lib/akr/')

# Akur8Engine git function
config = configparser.ConfigParser()
config.read(os.path.expanduser("~")+'/.gitconfig')
try:
    akr_user = config['user']['name']
    akr_email = config['user']['email']
except Exception as e:
    print("Akur8Engine (akr) required your name & email address to track"
          " changes you made under the Git version control")
    print("Akur8Engine (akr) will be able to send you daily reports & alerts in "
          "upcoming version")
    print("Akur8Engine (akr) will NEVER send your information across")

    akr_user = input("Enter your name: ")
    while akr_user is "":
        print("Name not Valid, Please enter again")
        akr_user = input("Enter your name: ")

    akr_email = input("Enter your email: ")

    while not re.match(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$",
                       akr_email):
        print("Invalid email address, please try again")
        akr_email = input("Enter your email: ")

    os.system("git config --global user.name {0}".format(akr_user))
    os.system("git config --global user.email {0}".format(akr_email))

if not os.path.isfile('/root/.gitconfig'):
      shutil.copy2(os.path.expanduser("~")+'/.gitconfig', '/root/.gitconfig')

setup(name='akr',
      version='3.4.1',
      description=long_description,
      long_description=long_description,
      classifiers=[],
      keywords='',
      author='rtCamp Soultions Pvt. LTD',
      author_email='akr@rtcamp.com',
      url='http://rtcamp.com/akur8engine',
      license='MIT',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests',
                                      'templates']),
      include_package_data=True,
      zip_safe=False,
      test_suite='nose.collector',
      install_requires=[
          # Required to build documentation
          # "Sphinx >= 1.0",
          # Required for testing
          # "nose",
          # "coverage",
          # Required to function
          'cement == 2.4',
          'pystache',
          'python-apt',
          'pynginxconfig',
          'pymysql3 == 0.4',
          'psutil == 3.1.1',
          'sh',
          'sqlalchemy',
          ],
      data_files=[('/etc/akr', ['config/akr.conf']),
                  ('/etc/akr/npextionsions.d', conf),
                  ('/usr/lib/akr/templates', templates),
                  ('/etc/bash_completion.d/',
                   ['config/bash_completion.d/akr_auto.rc']),
                  ('/usr/share/man/man8/', ['docs/akr.8'])],
      setup_requires=[],
      entry_points="""
          [console_scripts]
          akr = akr.cli.main:main
      """,
      namespace_packages=[],
      )
